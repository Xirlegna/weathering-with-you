import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Calendar from './Calendar';
import CitySearch from './UI/CitySearch/CitySearch';
import Spinner from './UI/Spinner/Spinner';
import Weather from './Weather/Weather';
import CitySearchPopupState from '../store/types/CitySearchPopupState';
import CityState from '../store/types/CityState';
import WeatherState from '../store/types/WeatherState';
import * as actions from '../store/actions/index';

const App: React.FC = () => {
    const dispatch = useDispatch();
    const onFetchWeather = (id: number) => dispatch(actions.fetchWeather(id));

    const city: CityState = useSelector(
        (state: { city: CityState }) => state.city
    );
    const citySearchPopup: CitySearchPopupState = useSelector(
        (state: { citySearchPopup: CitySearchPopupState }) => state.citySearchPopup
    );
    const weather: WeatherState = useSelector(
        (state: { weather: WeatherState }) => state.weather
    );

    useEffect(() => {
        onFetchWeather(city.id);
    }, [city]);

    return (
        <div className={['container', weather.className].join(' ')}>
            {weather.loading && <Spinner />}
            {citySearchPopup.display && <CitySearch />}
            <div className="container__header">
                <p className="container__title">Weathering With You</p>
            </div>
            <div className="container__body">
                <div className="container__calendar">
                    <Calendar />
                </div>
                <div className="container__weather">
                    <Weather />
                </div>
            </div>
        </div>
    );
};

export default App;