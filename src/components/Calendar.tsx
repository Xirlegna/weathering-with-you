import React, { useEffect, useState } from 'react';

const Calendar: React.FC = () => {
    const [time, setTime] = useState<string>('');
    const [date, setDate] = useState<string>('');

    const getDayName = (dayNum: number) => {
        let result = '';

        switch (dayNum) {
            case 0: result = 'Vasárna'; break;
            case 1: result = 'Hétfő'; break;
            case 2: result = 'Kedd'; break;
            case 3: result = 'Szerda'; break;
            case 4: result = 'Csütörtök'; break;
            case 5: result = 'Péntek'; break;
            case 6: result = 'Szombat'; break;
        }

        return result
    }

    const getMonthName = (monthNum: number) => {
        let result = '';

        switch (monthNum) {
            case 0: result = 'Január'; break;
            case 1: result = 'Február'; break;
            case 2: result = 'Március'; break;
            case 3: result = 'Április'; break;
            case 4: result = 'Május'; break;
            case 5: result = 'Június'; break;
            case 6: result = 'Július'; break;
            case 7: result = 'Augusztus'; break;
            case 8: result = 'Szeptember'; break;
            case 9: result = 'Október'; break;
            case 10: result = 'November'; break;
            case 11: result = 'December'; break;
        }

        return result
    }

    const startTimer = () => {
        setInterval(() => {
            const date = new Date();
            const hours = String(date.getHours()).padStart(2, '0');
            const minutes = String(date.getMinutes()).padStart(2, '0');
            setTime(`${hours}:${minutes}`);

            const dayName = getDayName(date.getDay());
            const monthName = getMonthName(date.getMonth());
            const dayInMonth = date.getDate();

            setDate(`${dayName} | ${monthName} ${dayInMonth}`);
        }, 1000);
    };

    useEffect(() => {
        startTimer();
    }, [startTimer]);

    return (
        <div className="calendar">
            <div className="calendar__time">{time}</div>
            <div className="calendar__date">{date}</div>
        </div>
    );
};

export default Calendar;