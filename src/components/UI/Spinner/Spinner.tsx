import React from 'react';

import Backdrop from '../Backdrop/Backdrop';

const Spinner: React.FC = () => (
    <Backdrop>
        <div className="spinner">Loading...</div>
    </Backdrop>
);

export default Spinner;
