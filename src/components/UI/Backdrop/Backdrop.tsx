import React from 'react';

const Backdrop: React.FC = ({ children }) => {
    return (
        <div className="backdrop">{children}</div>
    );
};

export default Backdrop;
