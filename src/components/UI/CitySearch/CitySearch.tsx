import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import Backdrop from '../Backdrop/Backdrop';
import cities from '../../../shared/cities';
import * as actions from '../../../store/actions/index';

const CitySearch: React.FC = () => {
    const [search, setSearch] = useState<string>('');

    const dispatch = useDispatch();
    const onCitySearchHide = () => dispatch(actions.citySearchHide());
    const onSetCity = (cityName: string) => dispatch(actions.setCity(cityName));

    const onClick = (cityName: string) => {
        onSetCity(cityName);
        onCitySearchHide();
    }

    const searchCities: JSX.Element[] = cities
        .sort(function (cityA, cityB) {
            const nameA: string = cityA.name.toUpperCase();
            const nameB: string = cityB.name.toUpperCase();
            if (nameA < nameB) { return -1; }
            if (nameA > nameB) { return 1; }
            return 0;
        })
        .filter((city) => city.name.toLowerCase().includes(search.toLowerCase()))
        .slice(0, 8)
        .map((city) => {
            return (
                <li className="city-search__list-item" onClick={() => onClick(city.name)}>{city.name}</li>
            )
        });

    return (
        <Backdrop>
            <div className="city-search">
                <div className="city-search__header">
                    <input
                        className="city-search__input"
                        value={search}
                        onChange={(e) => setSearch(e.target.value)}
                        type="text"
                        placeholder="Város"
                    />
                </div>
                <ul className="city-search__list">
                    {searchCities}
                </ul>
            </div>
        </Backdrop>
    );
};

export default CitySearch;
