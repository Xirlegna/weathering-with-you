import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Details from './Details/Details';
import * as actions from '../../store/actions/index';
import ClearIcon from '../../shared/icons/ClearIcon';
import CloudsIcon from '../../shared/icons/CloudsIcon';
import DrizzleIcon from '../../shared/icons/DrizzleIcon';
import FogIcon from '../../shared/icons/FogIcon';
import RainIcon from '../../shared/icons/RainIcon';
import SearchIcon from '../../shared/icons/SearchIcon';
import SnowIcon from '../../shared/icons/SnowIcon';
import SquallIcon from '../../shared/icons/SquallIcon';
import ThunderstormIcon from '../../shared/icons/ThunderstormIcon';
import TornadoIcon from '../../shared/icons/TornadoIcon';
import CityState from '../../store/types/CityState';
import WeatherState from '../../store/types/WeatherState';

const Weather: React.FC = () => {
    const city: CityState = useSelector(
        (state: { city: CityState }) => state.city
    );
    const weather: WeatherState = useSelector(
        (state: { weather: WeatherState }) => state.weather
    );

    const dispatch = useDispatch();
    const onCitySearchShow = () => dispatch(actions.citySearchShow());

    let icon: JSX.Element;

    switch (weather.data.main) {
        case 'Snow': icon = <SnowIcon className="weather__icon" />; break;
        case 'Thunderstorm': icon = <ThunderstormIcon className="weather__icon" />; break;
        case 'Clear': icon = <ClearIcon className="weather__icon" />; break;
        case 'Clouds': icon = <CloudsIcon className="weather__icon" />; break;
        case 'Rain': icon = <RainIcon className="weather__icon" />; break;
        case 'Drizzle': icon = <DrizzleIcon className="weather__icon" />; break;
        case 'Mist':
        case 'Smoke':
        case 'Haze':
        case 'Dust':
        case 'Fog':
        case 'Sand':
        case 'Dust':
        case 'Ash': icon = <FogIcon className="weather__icon" />; break;
        case 'Squall': icon = <SquallIcon className="weather__icon" />; break;
        case 'Tornado': icon = <TornadoIcon className="weather__icon" />; break;
        default: icon = <SnowIcon className="weather__icon" />;
    }

    return (
        <div className="weather">
            <div className="weather__header">
                <div className="weather__search" onClick={onCitySearchShow}><SearchIcon className="weather__search-icon" />{city.name}</div>
            </div>
            <div className="weather__body">
                <div className="weather__infos">
                    <div className="weather__main">
                        {icon}
                        <p className="weather__celsius">{Math.round(weather.data.temp)}&deg;</p>
                        <p className="weather__description">{weather.data.description}</p>
                    </div>
                    <Details />
                </div>
            </div>
        </div >
    );
};

export default Weather;