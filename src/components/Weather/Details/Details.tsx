import React from 'react';
import { useSelector } from 'react-redux';

import EyeIcon from '../../../shared/icons/EyeIcon';
import TemperatureIcon from '../../../shared/icons/TemperatureIcon';
import WindIcon from '../../../shared/icons/WindIcon';
import WeatherState from '../../../store/types/WeatherState';

const Details: React.FC = () => {
    const weather: WeatherState = useSelector(
        (state: { weather: WeatherState }) => state.weather
    );

    return (
        <div className="details">
            <div className="details__item">
                <TemperatureIcon className="details__icon" />
                <div className="details__text">
                    <p>Hőérzet</p>
                    <p>{Math.round(weather.data.feelsLike)}&deg;</p>
                </div>
            </div>
            <div className="details__item">
                <WindIcon className="details__icon" />
                <div className="details__text">
                    <p>Szél</p>
                    <p>{Math.round(weather.data.windSpeed)} km/ó</p>
                </div>
            </div>
            <div className="details__item">
                <EyeIcon className="details__icon" />
                <div className="details__text">
                    <p>Látótávolság</p>
                    <p>{Math.round(weather.data.visibility / 1000)} km</p>
                </div>
            </div>
        </div>
    );
};

export default Details;