const cities = [
    {
        "id": 713872,
        "name": "Zsellérföldek",
        "coord": {
            "lon": 21.033331,
            "lat": 46.816669
        }
    },
    {
        "id": 713929,
        "name": "Záhony",
        "coord": {
            "lon": 22.17614,
            "lat": 48.409061
        }
    },
    {
        "id": 713932,
        "name": "Zagyvarékas",
        "coord": {
            "lon": 20.133329,
            "lat": 47.26667
        }
    },
    {
        "id": 713949,
        "name": "Weinstocktanya",
        "coord": {
            "lon": 22.200001,
            "lat": 48.066669
        }
    },
    {
        "id": 713991,
        "name": "Vitéztelek",
        "coord": {
            "lon": 22.049999,
            "lat": 47.700001
        }
    },
    {
        "id": 714060,
        "name": "Vidólapos",
        "coord": {
            "lon": 20.83333,
            "lat": 46.933331
        }
    },
    {
        "id": 714073,
        "name": "Vésztő",
        "coord": {
            "lon": 21.26667,
            "lat": 46.916672
        }
    },
    {
        "id": 714081,
        "name": "Verpelét",
        "coord": {
            "lon": 20.23333,
            "lat": 47.849998
        }
    },
    {
        "id": 714119,
        "name": "Veresföld",
        "coord": {
            "lon": 21.66667,
            "lat": 48.400002
        }
    },
    {
        "id": 714144,
        "name": "Véghtanya",
        "coord": {
            "lon": 22.566669,
            "lat": 47.883331
        }
    },
    {
        "id": 714188,
        "name": "Vásárosnamény",
        "coord": {
            "lon": 22.316669,
            "lat": 48.133331
        }
    },
    {
        "id": 714258,
        "name": "Váraditag",
        "coord": {
            "lon": 22.133329,
            "lat": 47.833328
        }
    },
    {
        "id": 714265,
        "name": "Vámospércs",
        "coord": {
            "lon": 21.9,
            "lat": 47.533329
        }
    },
    {
        "id": 714296,
        "name": "Vaja",
        "coord": {
            "lon": 22.16761,
            "lat": 48.005741
        }
    },
    {
        "id": 714367,
        "name": "Újtelep",
        "coord": {
            "lon": 21.383329,
            "lat": 48.01667
        }
    },
    {
        "id": 714397,
        "name": "Újtanya",
        "coord": {
            "lon": 22.0,
            "lat": 47.966671
        }
    },
    {
        "id": 714419,
        "name": "Újszeged",
        "coord": {
            "lon": 20.16571,
            "lat": 46.24754
        }
    },
    {
        "id": 714421,
        "name": "Újszász",
        "coord": {
            "lon": 20.08333,
            "lat": 47.299999
        }
    },
    {
        "id": 714449,
        "name": "Újkígyós",
        "coord": {
            "lon": 21.033331,
            "lat": 46.583328
        }
    },
    {
        "id": 714464,
        "name": "Újfehértó",
        "coord": {
            "lon": 21.683331,
            "lat": 47.799999
        }
    },
    {
        "id": 714477,
        "name": "Ugaritanyák",
        "coord": {
            "lon": 21.033331,
            "lat": 46.816669
        }
    },
    {
        "id": 714478,
        "name": "Ugari Tanyák",
        "coord": {
            "lon": 20.200001,
            "lat": 47.183331
        }
    },
    {
        "id": 714487,
        "name": "Tyukodi Újtelep",
        "coord": {
            "lon": 22.549999,
            "lat": 47.849998
        }
    },
    {
        "id": 714488,
        "name": "Tyukod",
        "coord": {
            "lon": 22.563299,
            "lat": 47.853779
        }
    },
    {
        "id": 714490,
        "name": "Tuzsér",
        "coord": {
            "lon": 22.11762,
            "lat": 48.34407
        }
    },
    {
        "id": 714504,
        "name": "Túrkeve",
        "coord": {
            "lon": 20.75,
            "lat": 47.099998
        }
    },
    {
        "id": 714518,
        "name": "Tunyogmatolcs",
        "coord": {
            "lon": 22.466669,
            "lat": 47.966671
        }
    },
    {
        "id": 714546,
        "name": "Tótkomlósi Tanyák",
        "coord": {
            "lon": 20.73333,
            "lat": 46.416672
        }
    },
    {
        "id": 714548,
        "name": "Tótkomlós",
        "coord": {
            "lon": 20.73333,
            "lat": 46.416672
        }
    },
    {
        "id": 714552,
        "name": "Tóthtanya",
        "coord": {
            "lon": 20.41667,
            "lat": 47.75
        }
    },
    {
        "id": 714570,
        "name": "Tószeg",
        "coord": {
            "lon": 20.15,
            "lat": 47.099998
        }
    },
    {
        "id": 714581,
        "name": "Törökszentmiklós",
        "coord": {
            "lon": 20.41667,
            "lat": 47.183331
        }
    },
    {
        "id": 714593,
        "name": "Tornyospálca",
        "coord": {
            "lon": 22.183331,
            "lat": 48.26667
        }
    },
    {
        "id": 714623,
        "name": "Tömörkény",
        "coord": {
            "lon": 20.04357,
            "lat": 46.617161
        }
    },
    {
        "id": 714642,
        "name": "Tolcsva",
        "coord": {
            "lon": 21.450001,
            "lat": 48.283329
        }
    },
    {
        "id": 714659,
        "name": "Tokaj",
        "coord": {
            "lon": 21.41667,
            "lat": 48.116669
        }
    },
    {
        "id": 714692,
        "name": "Tiszavasvári",
        "coord": {
            "lon": 21.35,
            "lat": 47.966671
        }
    },
    {
        "id": 714697,
        "name": "Tiszaújváros",
        "coord": {
            "lon": 21.08333,
            "lat": 47.933331
        }
    },
    {
        "id": 714714,
        "name": "Tiszaszentimre",
        "coord": {
            "lon": 20.73333,
            "lat": 47.48333
        }
    },
    {
        "id": 714718,
        "name": "Tiszasüly",
        "coord": {
            "lon": 20.4,
            "lat": 47.383331
        }
    },
    {
        "id": 714721,
        "name": "Tiszaroff",
        "coord": {
            "lon": 20.450001,
            "lat": 47.400002
        }
    },
    {
        "id": 714724,
        "name": "Tiszapüspöki",
        "coord": {
            "lon": 20.316669,
            "lat": 47.216671
        }
    },
    {
        "id": 714734,
        "name": "Tiszanána",
        "coord": {
            "lon": 20.533331,
            "lat": 47.566669
        }
    },
    {
        "id": 714736,
        "name": "Tiszanagyfalu",
        "coord": {
            "lon": 21.48333,
            "lat": 48.099998
        }
    },
    {
        "id": 714739,
        "name": "Tiszalúc",
        "coord": {
            "lon": 21.08333,
            "lat": 48.033329
        }
    },
    {
        "id": 714742,
        "name": "Tiszalök",
        "coord": {
            "lon": 21.383329,
            "lat": 48.01667
        }
    },
    {
        "id": 714751,
        "name": "Tiszakeszi",
        "coord": {
            "lon": 21.0,
            "lat": 47.783329
        }
    },
    {
        "id": 714754,
        "name": "Tiszakécske",
        "coord": {
            "lon": 20.103491,
            "lat": 46.932621
        }
    },
    {
        "id": 714756,
        "name": "Tiszakarád",
        "coord": {
            "lon": 21.716669,
            "lat": 48.200001
        }
    },
    {
        "id": 714772,
        "name": "Tiszafüred",
        "coord": {
            "lon": 20.76667,
            "lat": 47.616669
        }
    },
    {
        "id": 714774,
        "name": "Tiszaföldvár",
        "coord": {
            "lon": 20.25,
            "lat": 46.98333
        }
    },
    {
        "id": 714776,
        "name": "Tiszaeszlár",
        "coord": {
            "lon": 21.466669,
            "lat": 48.049999
        }
    },
    {
        "id": 714779,
        "name": "Tiszadob",
        "coord": {
            "lon": 21.16667,
            "lat": 48.01667
        }
    },
    {
        "id": 714782,
        "name": "Tiszadada",
        "coord": {
            "lon": 21.25,
            "lat": 48.033329
        }
    },
    {
        "id": 714785,
        "name": "Tiszacsege",
        "coord": {
            "lon": 21.0,
            "lat": 47.700001
        }
    },
    {
        "id": 714787,
        "name": "Tiszabura",
        "coord": {
            "lon": 20.466669,
            "lat": 47.450001
        }
    },
    {
        "id": 714789,
        "name": "Tiszabő",
        "coord": {
            "lon": 20.48333,
            "lat": 47.299999
        }
    },
    {
        "id": 714791,
        "name": "Tiszabezdéd",
        "coord": {
            "lon": 22.15,
            "lat": 48.366669
        }
    },
    {
        "id": 714793,
        "name": "Tiszabercel",
        "coord": {
            "lon": 21.65,
            "lat": 48.150002
        }
    },
    {
        "id": 714816,
        "name": "Tibrik",
        "coord": {
            "lon": 20.383329,
            "lat": 47.900002
        }
    },
    {
        "id": 714914,
        "name": "Téglás",
        "coord": {
            "lon": 21.683331,
            "lat": 47.716671
        }
    },
    {
        "id": 714938,
        "name": "Tarpa",
        "coord": {
            "lon": 22.537439,
            "lat": 48.104801
        }
    },
    {
        "id": 714948,
        "name": "Tarnaörs",
        "coord": {
            "lon": 20.05254,
            "lat": 47.59499
        }
    },
    {
        "id": 714951,
        "name": "Tarnalelesz",
        "coord": {
            "lon": 20.183331,
            "lat": 48.049999
        }
    },
    {
        "id": 714973,
        "name": "Tarcal",
        "coord": {
            "lon": 21.35,
            "lat": 48.133331
        }
    },
    {
        "id": 715001,
        "name": "Tállya",
        "coord": {
            "lon": 21.23333,
            "lat": 48.23333
        }
    },
    {
        "id": 715005,
        "name": "Taktaszada",
        "coord": {
            "lon": 21.183331,
            "lat": 48.116669
        }
    },
    {
        "id": 715009,
        "name": "Taktaharkány",
        "coord": {
            "lon": 21.133329,
            "lat": 48.083328
        }
    },
    {
        "id": 715053,
        "name": "Szucsányitanya",
        "coord": {
            "lon": 22.216669,
            "lat": 47.916672
        }
    },
    {
        "id": 715126,
        "name": "Szolnok",
        "coord": {
            "lon": 20.200001,
            "lat": 47.183331
        }
    },
    {
        "id": 715177,
        "name": "Szoboszlaitanya",
        "coord": {
            "lon": 22.51667,
            "lat": 47.98333
        }
    },
    {
        "id": 715185,
        "name": "Szlobodatanya",
        "coord": {
            "lon": 21.466669,
            "lat": 48.133331
        }
    },
    {
        "id": 715189,
        "name": "Szivattyútelep",
        "coord": {
            "lon": 21.200001,
            "lat": 48.166672
        }
    },
    {
        "id": 715195,
        "name": "Szirtó",
        "coord": {
            "lon": 21.76667,
            "lat": 48.133331
        }
    },
    {
        "id": 715199,
        "name": "Szirmabesenyő",
        "coord": {
            "lon": 20.799999,
            "lat": 48.150002
        }
    },
    {
        "id": 715217,
        "name": "Szilvásvárad",
        "coord": {
            "lon": 20.4,
            "lat": 48.099998
        }
    },
    {
        "id": 715259,
        "name": "Szikszó",
        "coord": {
            "lon": 20.933331,
            "lat": 48.200001
        }
    },
    {
        "id": 715272,
        "name": "Szihalom",
        "coord": {
            "lon": 20.48333,
            "lat": 47.76667
        }
    },
    {
        "id": 715283,
        "name": "Sziget",
        "coord": {
            "lon": 22.51667,
            "lat": 47.916672
        }
    },
    {
        "id": 715296,
        "name": "Szeszgyárdűlő",
        "coord": {
            "lon": 20.1,
            "lat": 46.933331
        }
    },
    {
        "id": 715302,
        "name": "Szerencs",
        "coord": {
            "lon": 21.200001,
            "lat": 48.166672
        }
    },
    {
        "id": 715332,
        "name": "Szentistván",
        "coord": {
            "lon": 20.66667,
            "lat": 47.76667
        }
    },
    {
        "id": 715338,
        "name": "Szentes",
        "coord": {
            "lon": 20.26667,
            "lat": 46.650002
        }
    },
    {
        "id": 715355,
        "name": "Szendrő",
        "coord": {
            "lon": 20.73333,
            "lat": 48.400002
        }
    },
    {
        "id": 715393,
        "name": "Székkutas",
        "coord": {
            "lon": 20.533331,
            "lat": 46.5
        }
    },
    {
        "id": 715415,
        "name": "Szegvár",
        "coord": {
            "lon": 20.224079,
            "lat": 46.587399
        }
    },
    {
        "id": 715422,
        "name": "Szeghalom",
        "coord": {
            "lon": 21.16667,
            "lat": 47.033329
        }
    },
    {
        "id": 715423,
        "name": "Szegfűdűlő",
        "coord": {
            "lon": 21.4,
            "lat": 47.450001
        }
    },
    {
        "id": 715429,
        "name": "Szeged",
        "coord": {
            "lon": 20.148239,
            "lat": 46.252998
        }
    },
    {
        "id": 715449,
        "name": "Szatymaz",
        "coord": {
            "lon": 20.040199,
            "lat": 46.34306
        }
    },
    {
        "id": 715466,
        "name": "Szarvas",
        "coord": {
            "lon": 20.549999,
            "lat": 46.866669
        }
    },
    {
        "id": 715518,
        "name": "Szamosszeg",
        "coord": {
            "lon": 22.36582,
            "lat": 48.045609
        }
    },
    {
        "id": 715536,
        "name": "Szalmaré",
        "coord": {
            "lon": 21.216669,
            "lat": 47.099998
        }
    },
    {
        "id": 715558,
        "name": "Szakoly",
        "coord": {
            "lon": 21.91667,
            "lat": 47.76667
        }
    },
    {
        "id": 715570,
        "name": "Szajol",
        "coord": {
            "lon": 20.299999,
            "lat": 47.183331
        }
    },
    {
        "id": 715593,
        "name": "Szabolcs-Szatmár-Bereg",
        "coord": {
            "lon": 22.16667,
            "lat": 48.0
        }
    },
    {
        "id": 715614,
        "name": "Szabadkígyós",
        "coord": {
            "lon": 21.1,
            "lat": 46.616669
        }
    },
    {
        "id": 715763,
        "name": "Sirok",
        "coord": {
            "lon": 20.200001,
            "lat": 47.933331
        }
    },
    {
        "id": 715839,
        "name": "Sátoraljaújhely",
        "coord": {
            "lon": 21.66667,
            "lat": 48.400002
        }
    },
    {
        "id": 715862,
        "name": "Sárrétudvari",
        "coord": {
            "lon": 21.200001,
            "lat": 47.23333
        }
    },
    {
        "id": 715867,
        "name": "Sárospatak",
        "coord": {
            "lon": 21.58333,
            "lat": 48.316669
        }
    },
    {
        "id": 715888,
        "name": "Sarkad",
        "coord": {
            "lon": 21.383329,
            "lat": 46.75
        }
    },
    {
        "id": 715904,
        "name": "Sáránd",
        "coord": {
            "lon": 21.633329,
            "lat": 47.400002
        }
    },
    {
        "id": 715924,
        "name": "Sándorfalva",
        "coord": {
            "lon": 20.09889,
            "lat": 46.36087
        }
    },
    {
        "id": 715930,
        "name": "Sály",
        "coord": {
            "lon": 20.66667,
            "lat": 47.950001
        }
    },
    {
        "id": 715939,
        "name": "Sajóvámos",
        "coord": {
            "lon": 20.85,
            "lat": 48.183331
        }
    },
    {
        "id": 715941,
        "name": "Sajószöged",
        "coord": {
            "lon": 21.0,
            "lat": 47.950001
        }
    },
    {
        "id": 715945,
        "name": "Sajószentpéter",
        "coord": {
            "lon": 20.716669,
            "lat": 48.216671
        }
    },
    {
        "id": 715951,
        "name": "Sajóörös",
        "coord": {
            "lon": 21.033331,
            "lat": 47.950001
        }
    },
    {
        "id": 715958,
        "name": "Sajólád",
        "coord": {
            "lon": 20.9,
            "lat": 48.049999
        }
    },
    {
        "id": 715962,
        "name": "Sajókaza",
        "coord": {
            "lon": 20.58333,
            "lat": 48.283329
        }
    },
    {
        "id": 715970,
        "name": "Sajóbábony",
        "coord": {
            "lon": 20.73333,
            "lat": 48.166672
        }
    },
    {
        "id": 715975,
        "name": "Ruzsiczkytanya",
        "coord": {
            "lon": 21.200001,
            "lat": 48.166672
        }
    },
    {
        "id": 715976,
        "name": "Ruzsatanya",
        "coord": {
            "lon": 22.16667,
            "lat": 48.0
        }
    },
    {
        "id": 715989,
        "name": "Rudabánya",
        "coord": {
            "lon": 20.633329,
            "lat": 48.383331
        }
    },
    {
        "id": 716028,
        "name": "Röszke",
        "coord": {
            "lon": 20.03372,
            "lat": 46.187962
        }
    },
    {
        "id": 716081,
        "name": "Ricse",
        "coord": {
            "lon": 21.97069,
            "lat": 48.325642
        }
    },
    {
        "id": 716151,
        "name": "Recsk",
        "coord": {
            "lon": 20.116671,
            "lat": 47.933331
        }
    },
    {
        "id": 716186,
        "name": "Rákócziújfalu",
        "coord": {
            "lon": 20.26667,
            "lat": 47.066669
        }
    },
    {
        "id": 716197,
        "name": "Rákóczifalva",
        "coord": {
            "lon": 20.23333,
            "lat": 47.083328
        }
    },
    {
        "id": 716204,
        "name": "Rakamaz",
        "coord": {
            "lon": 21.466669,
            "lat": 48.133331
        }
    },
    {
        "id": 716247,
        "name": "Putnok",
        "coord": {
            "lon": 20.433331,
            "lat": 48.299999
        }
    },
    {
        "id": 716282,
        "name": "Pusztaföldvár",
        "coord": {
            "lon": 20.799999,
            "lat": 46.533329
        }
    },
    {
        "id": 716301,
        "name": "Püspökladány",
        "coord": {
            "lon": 21.116671,
            "lat": 47.316669
        }
    },
    {
        "id": 716317,
        "name": "Prügy",
        "coord": {
            "lon": 21.25,
            "lat": 48.083328
        }
    },
    {
        "id": 716339,
        "name": "Poroszló",
        "coord": {
            "lon": 20.66667,
            "lat": 47.650002
        }
    },
    {
        "id": 716343,
        "name": "Porcsalma",
        "coord": {
            "lon": 22.566669,
            "lat": 47.883331
        }
    },
    {
        "id": 716356,
        "name": "Polgár",
        "coord": {
            "lon": 21.116671,
            "lat": 47.866669
        }
    },
    {
        "id": 716372,
        "name": "Pocsaj",
        "coord": {
            "lon": 21.816669,
            "lat": 47.283329
        }
    },
    {
        "id": 716437,
        "name": "Petneháza",
        "coord": {
            "lon": 22.07485,
            "lat": 48.060841
        }
    },
    {
        "id": 716439,
        "name": "Pétervására",
        "coord": {
            "lon": 20.1,
            "lat": 48.01667
        }
    },
    {
        "id": 716480,
        "name": "Peregpuszta",
        "coord": {
            "lon": 20.816669,
            "lat": 46.316669
        }
    },
    {
        "id": 716526,
        "name": "Pátroha",
        "coord": {
            "lon": 22.0,
            "lat": 48.166672
        }
    },
    {
        "id": 716559,
        "name": "Parád",
        "coord": {
            "lon": 20.02972,
            "lat": 47.923241
        }
    },
    {
        "id": 716603,
        "name": "Pap Elek-tanya",
        "coord": {
            "lon": 20.91667,
            "lat": 46.400002
        }
    },
    {
        "id": 716611,
        "name": "Pankotaitanya",
        "coord": {
            "lon": 22.26667,
            "lat": 47.916672
        }
    },
    {
        "id": 716671,
        "name": "Ózd",
        "coord": {
            "lon": 20.299999,
            "lat": 48.216671
        }
    },
    {
        "id": 716686,
        "name": "Ószőlőskert",
        "coord": {
            "lon": 21.933331,
            "lat": 47.866669
        }
    },
    {
        "id": 716706,
        "name": "Ostoros",
        "coord": {
            "lon": 20.433331,
            "lat": 47.866669
        }
    },
    {
        "id": 716717,
        "name": "Orsószög",
        "coord": {
            "lon": 21.9,
            "lat": 48.25
        }
    },
    {
        "id": 716736,
        "name": "Orosháza",
        "coord": {
            "lon": 20.66667,
            "lat": 46.566669
        }
    },
    {
        "id": 716791,
        "name": "Ópusztaszer",
        "coord": {
            "lon": 20.087219,
            "lat": 46.48592
        }
    },
    {
        "id": 716793,
        "name": "Ópályi",
        "coord": {
            "lon": 22.32617,
            "lat": 47.997711
        }
    },
    {
        "id": 716796,
        "name": "Ónod",
        "coord": {
            "lon": 20.91667,
            "lat": 48.0
        }
    },
    {
        "id": 716799,
        "name": "Onga",
        "coord": {
            "lon": 20.91667,
            "lat": 48.116669
        }
    },
    {
        "id": 716828,
        "name": "Olaszliszka",
        "coord": {
            "lon": 21.433331,
            "lat": 48.25
        }
    },
    {
        "id": 716844,
        "name": "Ököritófülpös",
        "coord": {
            "lon": 22.508101,
            "lat": 47.918621
        }
    },
    {
        "id": 716853,
        "name": "Okány",
        "coord": {
            "lon": 21.35,
            "lat": 46.900002
        }
    },
    {
        "id": 716860,
        "name": "Ófehértó",
        "coord": {
            "lon": 22.049999,
            "lat": 47.933331
        }
    },
    {
        "id": 716864,
        "name": "Öcsöd",
        "coord": {
            "lon": 20.4,
            "lat": 46.900002
        }
    },
    {
        "id": 716877,
        "name": "Nyulasszőllő",
        "coord": {
            "lon": 20.283331,
            "lat": 47.599998
        }
    },
    {
        "id": 716889,
        "name": "Nyírvasvári",
        "coord": {
            "lon": 22.186831,
            "lat": 47.816669
        }
    },
    {
        "id": 716893,
        "name": "Nyírtelek",
        "coord": {
            "lon": 21.633329,
            "lat": 48.01667
        }
    },
    {
        "id": 716894,
        "name": "Nyírtass",
        "coord": {
            "lon": 22.033331,
            "lat": 48.116669
        }
    },
    {
        "id": 716899,
        "name": "Nyírpazony",
        "coord": {
            "lon": 21.799999,
            "lat": 47.98333
        }
    },
    {
        "id": 716903,
        "name": "Nyírmihálydi",
        "coord": {
            "lon": 21.964451,
            "lat": 47.739761
        }
    },
    {
        "id": 716905,
        "name": "Nyírmeggyes",
        "coord": {
            "lon": 22.26667,
            "lat": 47.916672
        }
    },
    {
        "id": 716906,
        "name": "Nyírmártonfalva",
        "coord": {
            "lon": 21.9,
            "lat": 47.583328
        }
    },
    {
        "id": 716908,
        "name": "Nyírmada",
        "coord": {
            "lon": 22.200001,
            "lat": 48.066669
        }
    },
    {
        "id": 716909,
        "name": "Nyírlugos",
        "coord": {
            "lon": 22.04476,
            "lat": 47.69315
        }
    },
    {
        "id": 716913,
        "name": "Nyírkarász",
        "coord": {
            "lon": 22.09794,
            "lat": 48.101669
        }
    },
    {
        "id": 716924,
        "name": "Nyírgyulaj",
        "coord": {
            "lon": 22.097811,
            "lat": 47.886219
        }
    },
    {
        "id": 716935,
        "name": "Nyíregyháza",
        "coord": {
            "lon": 21.716709,
            "lat": 47.955391
        }
    },
    {
        "id": 716941,
        "name": "Nyírcsaholy",
        "coord": {
            "lon": 22.3363,
            "lat": 47.903831
        }
    },
    {
        "id": 716943,
        "name": "Nyírbogdány",
        "coord": {
            "lon": 21.88242,
            "lat": 48.057232
        }
    },
    {
        "id": 716945,
        "name": "Nyírbogát",
        "coord": {
            "lon": 22.065611,
            "lat": 47.803398
        }
    },
    {
        "id": 716946,
        "name": "Nyírbéltek",
        "coord": {
            "lon": 22.133329,
            "lat": 47.700001
        }
    },
    {
        "id": 716948,
        "name": "Nyírbátor",
        "coord": {
            "lon": 22.133329,
            "lat": 47.833328
        }
    },
    {
        "id": 716949,
        "name": "Nyirántanya",
        "coord": {
            "lon": 22.683331,
            "lat": 47.833328
        }
    },
    {
        "id": 716951,
        "name": "Nyíradony",
        "coord": {
            "lon": 21.918779,
            "lat": 47.69746
        }
    },
    {
        "id": 716952,
        "name": "Nyíracsád",
        "coord": {
            "lon": 21.97208,
            "lat": 47.603298
        }
    },
    {
        "id": 716954,
        "name": "Nyírábrány",
        "coord": {
            "lon": 22.02401,
            "lat": 47.553108
        }
    },
    {
        "id": 716963,
        "name": "Nyerjesszőllő",
        "coord": {
            "lon": 22.08333,
            "lat": 48.0
        }
    },
    {
        "id": 716968,
        "name": "Nyékládháza",
        "coord": {
            "lon": 20.83333,
            "lat": 47.98333
        }
    },
    {
        "id": 717016,
        "name": "Németi",
        "coord": {
            "lon": 21.26667,
            "lat": 46.916672
        }
    },
    {
        "id": 717042,
        "name": "Napkor",
        "coord": {
            "lon": 21.86763,
            "lat": 47.937969
        }
    },
    {
        "id": 717118,
        "name": "Nagyszőllő",
        "coord": {
            "lon": 22.066669,
            "lat": 48.183331
        }
    },
    {
        "id": 717130,
        "name": "Nagyszénás",
        "coord": {
            "lon": 20.66667,
            "lat": 46.683331
        }
    },
    {
        "id": 717172,
        "name": "Nagyrábé",
        "coord": {
            "lon": 21.33333,
            "lat": 47.200001
        }
    },
    {
        "id": 717281,
        "name": "Nagykálló",
        "coord": {
            "lon": 21.84082,
            "lat": 47.874908
        }
    },
    {
        "id": 717345,
        "name": "Nagyhalász",
        "coord": {
            "lon": 21.76667,
            "lat": 48.133331
        }
    },
    {
        "id": 717357,
        "name": "Nagygajla",
        "coord": {
            "lon": 20.91667,
            "lat": 47.816669
        }
    },
    {
        "id": 717397,
        "name": "Nagyecsed",
        "coord": {
            "lon": 22.3916,
            "lat": 47.865471
        }
    },
    {
        "id": 717400,
        "name": "Nagydobos",
        "coord": {
            "lon": 22.304239,
            "lat": 48.05759
        }
    },
    {
        "id": 717406,
        "name": "Nagycserkesz",
        "coord": {
            "lon": 21.533331,
            "lat": 47.966671
        }
    },
    {
        "id": 717449,
        "name": "Nádudvar",
        "coord": {
            "lon": 21.16667,
            "lat": 47.416672
        }
    },
    {
        "id": 717497,
        "name": "Múcsony",
        "coord": {
            "lon": 20.683331,
            "lat": 48.26667
        }
    },
    {
        "id": 717521,
        "name": "Monostorpályi",
        "coord": {
            "lon": 21.783331,
            "lat": 47.400002
        }
    },
    {
        "id": 717530,
        "name": "Monok",
        "coord": {
            "lon": 21.15,
            "lat": 48.216671
        }
    },
    {
        "id": 717570,
        "name": "Mocsolya",
        "coord": {
            "lon": 22.033331,
            "lat": 47.883331
        }
    },
    {
        "id": 717582,
        "name": "Miskolc",
        "coord": {
            "lon": 20.783331,
            "lat": 48.099998
        }
    },
    {
        "id": 717591,
        "name": "Mindszent",
        "coord": {
            "lon": 20.19038,
            "lat": 46.523621
        }
    },
    {
        "id": 717612,
        "name": "Mikitatanya",
        "coord": {
            "lon": 21.6,
            "lat": 48.166672
        }
    },
    {
        "id": 717616,
        "name": "Mikepércs",
        "coord": {
            "lon": 21.633329,
            "lat": 47.450001
        }
    },
    {
        "id": 717632,
        "name": "Mezőzombor",
        "coord": {
            "lon": 21.26667,
            "lat": 48.150002
        }
    },
    {
        "id": 717635,
        "name": "Mezőtúr",
        "coord": {
            "lon": 20.633329,
            "lat": 47.0
        }
    },
    {
        "id": 717642,
        "name": "Mezőssytag",
        "coord": {
            "lon": 21.683331,
            "lat": 47.799999
        }
    },
    {
        "id": 717652,
        "name": "Mezőkövesd",
        "coord": {
            "lon": 20.58333,
            "lat": 47.816669
        }
    },
    {
        "id": 717656,
        "name": "Mezőkovácsháza",
        "coord": {
            "lon": 20.91667,
            "lat": 46.400002
        }
    },
    {
        "id": 717658,
        "name": "Mezőkeresztes",
        "coord": {
            "lon": 20.700001,
            "lat": 47.833328
        }
    },
    {
        "id": 717662,
        "name": "Mezőhegyes",
        "coord": {
            "lon": 20.816669,
            "lat": 46.316669
        }
    },
    {
        "id": 717667,
        "name": "Mezőcsát",
        "coord": {
            "lon": 20.91667,
            "lat": 47.816669
        }
    },
    {
        "id": 717669,
        "name": "Mezőberény",
        "coord": {
            "lon": 21.033331,
            "lat": 46.816669
        }
    },
    {
        "id": 717693,
        "name": "Mérk",
        "coord": {
            "lon": 22.380381,
            "lat": 47.788239
        }
    },
    {
        "id": 717694,
        "name": "Mérgeszug",
        "coord": {
            "lon": 21.08333,
            "lat": 46.966671
        }
    },
    {
        "id": 717721,
        "name": "Méhkerék",
        "coord": {
            "lon": 21.450001,
            "lat": 46.783329
        }
    },
    {
        "id": 717726,
        "name": "Megyaszó",
        "coord": {
            "lon": 21.049999,
            "lat": 48.183331
        }
    },
    {
        "id": 717741,
        "name": "Medgyesegyháza",
        "coord": {
            "lon": 21.033331,
            "lat": 46.5
        }
    },
    {
        "id": 717758,
        "name": "Mátraderecske",
        "coord": {
            "lon": 20.08333,
            "lat": 47.950001
        }
    },
    {
        "id": 717771,
        "name": "Mátészalka",
        "coord": {
            "lon": 22.323481,
            "lat": 47.95528
        }
    },
    {
        "id": 717779,
        "name": "Második Rozsréti Bokor",
        "coord": {
            "lon": 21.716669,
            "lat": 47.950001
        }
    },
    {
        "id": 717805,
        "name": "Maroslele",
        "coord": {
            "lon": 20.35,
            "lat": 46.26667
        }
    },
    {
        "id": 717825,
        "name": "Máriapócs",
        "coord": {
            "lon": 22.025021,
            "lat": 47.883018
        }
    },
    {
        "id": 717833,
        "name": "Máriafürdő",
        "coord": {
            "lon": 20.433331,
            "lat": 48.299999
        }
    },
    {
        "id": 717844,
        "name": "Maradékdűlő",
        "coord": {
            "lon": 20.549999,
            "lat": 46.866669
        }
    },
    {
        "id": 717853,
        "name": "Mándok",
        "coord": {
            "lon": 22.191071,
            "lat": 48.321491
        }
    },
    {
        "id": 717861,
        "name": "Mályi",
        "coord": {
            "lon": 20.83333,
            "lat": 48.01667
        }
    },
    {
        "id": 717883,
        "name": "Malombirtokosság",
        "coord": {
            "lon": 20.73333,
            "lat": 48.299999
        }
    },
    {
        "id": 717902,
        "name": "Makó",
        "coord": {
            "lon": 20.48333,
            "lat": 46.216671
        }
    },
    {
        "id": 717904,
        "name": "Maklár",
        "coord": {
            "lon": 20.41667,
            "lat": 47.799999
        }
    },
    {
        "id": 717936,
        "name": "Magyardombegyházi Dűlő",
        "coord": {
            "lon": 21.01667,
            "lat": 46.283329
        }
    },
    {
        "id": 717943,
        "name": "Magyarbánhegyes",
        "coord": {
            "lon": 20.966669,
            "lat": 46.450001
        }
    },
    {
        "id": 717980,
        "name": "Mád",
        "coord": {
            "lon": 21.283331,
            "lat": 48.200001
        }
    },
    {
        "id": 718026,
        "name": "Ludaddűlő",
        "coord": {
            "lon": 21.133329,
            "lat": 46.76667
        }
    },
    {
        "id": 718045,
        "name": "Lórántffykert",
        "coord": {
            "lon": 21.51667,
            "lat": 47.666672
        }
    },
    {
        "id": 718058,
        "name": "Lökösihatár",
        "coord": {
            "lon": 21.25,
            "lat": 46.533329
        }
    },
    {
        "id": 718060,
        "name": "Lőkösháza",
        "coord": {
            "lon": 21.23333,
            "lat": 46.433331
        }
    },
    {
        "id": 718075,
        "name": "North Macedonia",
        "country": "MK",
        "coord": {
            "lon": 22.0,
            "lat": 41.833328
        }
    },
    {
        "id": 718108,
        "name": "Levelek",
        "coord": {
            "lon": 21.985371,
            "lat": 47.962818
        }
    },
    {
        "id": 718113,
        "name": "Létavértes",
        "coord": {
            "lon": 21.9,
            "lat": 47.383331
        }
    },
    {
        "id": 718180,
        "name": "Lászlótanya",
        "coord": {
            "lon": 21.91667,
            "lat": 48.116669
        }
    },
    {
        "id": 718214,
        "name": "Lapistó",
        "coord": {
            "lon": 20.26667,
            "lat": 46.650002
        }
    },
    {
        "id": 718312,
        "name": "Kunszentmárton",
        "coord": {
            "lon": 20.28879,
            "lat": 46.839161
        }
    },
    {
        "id": 718315,
        "name": "Kunmadaras",
        "coord": {
            "lon": 20.799999,
            "lat": 47.433331
        }
    },
    {
        "id": 718318,
        "name": "Kunlapos",
        "coord": {
            "lon": 21.116671,
            "lat": 47.316669
        }
    },
    {
        "id": 718321,
        "name": "Kunhegyes",
        "coord": {
            "lon": 20.633329,
            "lat": 47.366669
        }
    },
    {
        "id": 718334,
        "name": "Kunágota",
        "coord": {
            "lon": 21.049999,
            "lat": 46.433331
        }
    },
    {
        "id": 718480,
        "name": "Kótaj",
        "coord": {
            "lon": 21.716669,
            "lat": 48.049999
        }
    },
    {
        "id": 718516,
        "name": "Köröstarcsa",
        "coord": {
            "lon": 21.033331,
            "lat": 46.883331
        }
    },
    {
        "id": 718528,
        "name": "Körösladány",
        "coord": {
            "lon": 21.08333,
            "lat": 46.966671
        }
    },
    {
        "id": 718602,
        "name": "Konyár",
        "coord": {
            "lon": 21.66667,
            "lat": 47.316669
        }
    },
    {
        "id": 718615,
        "name": "Kondoros",
        "coord": {
            "lon": 20.799999,
            "lat": 46.76667
        }
    },
    {
        "id": 718618,
        "name": "Kompolt",
        "coord": {
            "lon": 20.25,
            "lat": 47.73333
        }
    },
    {
        "id": 718637,
        "name": "Komádi",
        "coord": {
            "lon": 21.5,
            "lat": 47.0
        }
    },
    {
        "id": 718653,
        "name": "Kölcsei Kendergyár",
        "coord": {
            "lon": 22.716669,
            "lat": 48.049999
        }
    },
    {
        "id": 718699,
        "name": "Kocsord",
        "coord": {
            "lon": 22.383329,
            "lat": 47.939121
        }
    },
    {
        "id": 718723,
        "name": "Kiszombor",
        "coord": {
            "lon": 20.433331,
            "lat": 46.183331
        }
    },
    {
        "id": 718739,
        "name": "Kisvárda",
        "coord": {
            "lon": 22.08333,
            "lat": 48.216671
        }
    },
    {
        "id": 718744,
        "name": "Kisújszállás",
        "coord": {
            "lon": 20.76667,
            "lat": 47.216671
        }
    },
    {
        "id": 718780,
        "name": "Kistanya",
        "coord": {
            "lon": 21.85,
            "lat": 47.883331
        }
    },
    {
        "id": 718882,
        "name": "Kismező",
        "coord": {
            "lon": 21.51667,
            "lat": 48.466671
        }
    },
    {
        "id": 718897,
        "name": "Kisléta",
        "coord": {
            "lon": 22.003929,
            "lat": 47.842442
        }
    },
    {
        "id": 718911,
        "name": "Kisköre",
        "coord": {
            "lon": 20.5,
            "lat": 47.5
        }
    },
    {
        "id": 719093,
        "name": "Kevermes",
        "coord": {
            "lon": 21.183331,
            "lat": 46.416672
        }
    },
    {
        "id": 719126,
        "name": "Kétegyháza",
        "coord": {
            "lon": 21.183331,
            "lat": 46.533329
        }
    },
    {
        "id": 719211,
        "name": "Kerecsend",
        "coord": {
            "lon": 20.35,
            "lat": 47.799999
        }
    },
    {
        "id": 719232,
        "name": "Kengyel",
        "coord": {
            "lon": 20.33333,
            "lat": 47.083328
        }
    },
    {
        "id": 719252,
        "name": "Kenderes",
        "coord": {
            "lon": 20.683331,
            "lat": 47.25
        }
    },
    {
        "id": 719260,
        "name": "Kemecse",
        "coord": {
            "lon": 21.80625,
            "lat": 48.075321
        }
    },
    {
        "id": 719280,
        "name": "Kék",
        "coord": {
            "lon": 21.883329,
            "lat": 48.116669
        }
    },
    {
        "id": 719311,
        "name": "Kazincbarcika",
        "coord": {
            "lon": 20.633329,
            "lat": 48.25
        }
    },
    {
        "id": 719317,
        "name": "Kavics-banya",
        "coord": {
            "lon": 20.883329,
            "lat": 48.066669
        }
    },
    {
        "id": 719346,
        "name": "Kaszaper",
        "coord": {
            "lon": 20.83333,
            "lat": 46.466671
        }
    },
    {
        "id": 719396,
        "name": "Karcsa",
        "coord": {
            "lon": 21.80537,
            "lat": 48.311298
        }
    },
    {
        "id": 719404,
        "name": "Karcag",
        "coord": {
            "lon": 20.933331,
            "lat": 47.316669
        }
    },
    {
        "id": 719419,
        "name": "Karácsond",
        "coord": {
            "lon": 20.03076,
            "lat": 47.729622
        }
    },
    {
        "id": 719466,
        "name": "Kántorjánosi",
        "coord": {
            "lon": 22.15,
            "lat": 47.933331
        }
    },
    {
        "id": 719482,
        "name": "Kálvária",
        "coord": {
            "lon": 21.283331,
            "lat": 46.650002
        }
    },
    {
        "id": 719489,
        "name": "Kálmánháza",
        "coord": {
            "lon": 21.58333,
            "lat": 47.883331
        }
    },
    {
        "id": 719493,
        "name": "Kállósemjén",
        "coord": {
            "lon": 21.93928,
            "lat": 47.860809
        }
    },
    {
        "id": 719511,
        "name": "Kál",
        "coord": {
            "lon": 20.26667,
            "lat": 47.73333
        }
    },
    {
        "id": 719555,
        "name": "Kaba",
        "coord": {
            "lon": 21.283331,
            "lat": 47.349998
        }
    },
    {
        "id": 719635,
        "name": "Jászszentandrás",
        "coord": {
            "lon": 20.183331,
            "lat": 47.583328
        }
    },
    {
        "id": 719637,
        "name": "Jász-Nagykun-Szolnok",
        "coord": {
            "lon": 20.5,
            "lat": 47.25
        }
    },
    {
        "id": 719640,
        "name": "Jászladány",
        "coord": {
            "lon": 20.16667,
            "lat": 47.366669
        }
    },
    {
        "id": 719643,
        "name": "Jászkisér",
        "coord": {
            "lon": 20.216669,
            "lat": 47.450001
        }
    },
    {
        "id": 719644,
        "name": "Jászkarajenő",
        "coord": {
            "lon": 20.066669,
            "lat": 47.049999
        }
    },
    {
        "id": 719645,
        "name": "Jászjákóhalma",
        "coord": {
            "lon": 19.99086,
            "lat": 47.520378
        }
    },
    {
        "id": 719649,
        "name": "Jászdózsa",
        "coord": {
            "lon": 20.015341,
            "lat": 47.566101
        }
    },
    {
        "id": 719655,
        "name": "Jászapáti",
        "coord": {
            "lon": 20.15,
            "lat": 47.51667
        }
    },
    {
        "id": 719656,
        "name": "Jászalsószentgyörgy",
        "coord": {
            "lon": 20.1,
            "lat": 47.366669
        }
    },
    {
        "id": 719668,
        "name": "Járdánháza",
        "coord": {
            "lon": 20.25,
            "lat": 48.150002
        }
    },
    {
        "id": 719678,
        "name": "Jánoshida",
        "coord": {
            "lon": 20.066669,
            "lat": 47.383331
        }
    },
    {
        "id": 719700,
        "name": "Jakabtanya",
        "coord": {
            "lon": 20.933331,
            "lat": 48.200001
        }
    },
    {
        "id": 719710,
        "name": "Izsófalva",
        "coord": {
            "lon": 20.66667,
            "lat": 48.299999
        }
    },
    {
        "id": 719717,
        "name": "Ivánkapuszta",
        "coord": {
            "lon": 20.73333,
            "lat": 48.400002
        }
    },
    {
        "id": 719806,
        "name": "Ibrány",
        "coord": {
            "lon": 21.716669,
            "lat": 48.133331
        }
    },
    {
        "id": 719819,
        "name": "Hungary",
        "coord": {
            "lon": 20.0,
            "lat": 47.0
        }
    },
    {
        "id": 719830,
        "name": "Hosztán",
        "coord": {
            "lon": 21.76667,
            "lat": 48.133331
        }
    },
    {
        "id": 719843,
        "name": "Hosszúpályi",
        "coord": {
            "lon": 21.7328,
            "lat": 47.393028
        }
    },
    {
        "id": 719895,
        "name": "Hortobágy",
        "coord": {
            "lon": 21.151079,
            "lat": 47.582779
        }
    },
    {
        "id": 719965,
        "name": "Hódmezővásárhely",
        "coord": {
            "lon": 20.33333,
            "lat": 46.416672
        }
    },
    {
        "id": 719967,
        "name": "Hodász",
        "coord": {
            "lon": 22.20153,
            "lat": 47.918339
        }
    },
    {
        "id": 720002,
        "name": "Heves megye",
        "coord": {
            "lon": 20.25,
            "lat": 47.833328
        }
    },
    {
        "id": 720006,
        "name": "Heves",
        "coord": {
            "lon": 20.283331,
            "lat": 47.599998
        }
    },
    {
        "id": 720026,
        "name": "Hernádnémeti",
        "coord": {
            "lon": 20.98333,
            "lat": 48.066669
        }
    },
    {
        "id": 720061,
        "name": "Hejőbába",
        "coord": {
            "lon": 20.950001,
            "lat": 47.900002
        }
    },
    {
        "id": 720064,
        "name": "Hejce",
        "coord": {
            "lon": 21.283331,
            "lat": 48.433331
        }
    },
    {
        "id": 720149,
        "name": "Harsány",
        "coord": {
            "lon": 20.75,
            "lat": 47.966671
        }
    },
    {
        "id": 720174,
        "name": "Harkányidűlő",
        "coord": {
            "lon": 20.133329,
            "lat": 47.083328
        }
    },
    {
        "id": 720228,
        "name": "Halmaj",
        "coord": {
            "lon": 21.0,
            "lat": 48.25
        }
    },
    {
        "id": 720274,
        "name": "Hajdúszovát",
        "coord": {
            "lon": 21.48333,
            "lat": 47.383331
        }
    },
    {
        "id": 720276,
        "name": "Hajdúszoboszló",
        "coord": {
            "lon": 21.4,
            "lat": 47.450001
        }
    },
    {
        "id": 720280,
        "name": "Hajdúsámson",
        "coord": {
            "lon": 21.76667,
            "lat": 47.599998
        }
    },
    {
        "id": 720284,
        "name": "Hajdúnánás",
        "coord": {
            "lon": 21.433331,
            "lat": 47.849998
        }
    },
    {
        "id": 720287,
        "name": "Hajdúhadház",
        "coord": {
            "lon": 21.66667,
            "lat": 47.683331
        }
    },
    {
        "id": 720290,
        "name": "Hajdúdorog",
        "coord": {
            "lon": 21.5,
            "lat": 47.816669
        }
    },
    {
        "id": 720292,
        "name": "Hajdúböszörmény",
        "coord": {
            "lon": 21.51667,
            "lat": 47.666672
        }
    },
    {
        "id": 720293,
        "name": "Hajdú-Bihar",
        "coord": {
            "lon": 21.5,
            "lat": 47.416672
        }
    },
    {
        "id": 720295,
        "name": "Hajdúbagos",
        "coord": {
            "lon": 21.66551,
            "lat": 47.392948
        }
    },
    {
        "id": 720330,
        "name": "Gyulaháza",
        "coord": {
            "lon": 22.116671,
            "lat": 48.133331
        }
    },
    {
        "id": 720334,
        "name": "Gyula",
        "coord": {
            "lon": 21.283331,
            "lat": 46.650002
        }
    },
    {
        "id": 720364,
        "name": "Gyomaendrőd",
        "coord": {
            "lon": 20.83333,
            "lat": 46.933331
        }
    },
    {
        "id": 720417,
        "name": "Gulyalegelő",
        "coord": {
            "lon": 21.58333,
            "lat": 48.316669
        }
    },
    {
        "id": 720480,
        "name": "Görbeháza",
        "coord": {
            "lon": 21.23333,
            "lat": 47.833328
        }
    },
    {
        "id": 720494,
        "name": "Gönc",
        "coord": {
            "lon": 21.283331,
            "lat": 48.466671
        }
    },
    {
        "id": 720533,
        "name": "Gesztely",
        "coord": {
            "lon": 20.966669,
            "lat": 48.099998
        }
    },
    {
        "id": 720547,
        "name": "Gergelyffinagytanya",
        "coord": {
            "lon": 22.033331,
            "lat": 47.883331
        }
    },
    {
        "id": 720574,
        "name": "Gégény",
        "coord": {
            "lon": 21.950001,
            "lat": 48.150002
        }
    },
    {
        "id": 720604,
        "name": "Garadna",
        "coord": {
            "lon": 21.183331,
            "lat": 48.416672
        }
    },
    {
        "id": 720643,
        "name": "Gádoros",
        "coord": {
            "lon": 20.6,
            "lat": 46.666672
        }
    },
    {
        "id": 720679,
        "name": "Füzesgyarmat",
        "coord": {
            "lon": 21.216669,
            "lat": 47.099998
        }
    },
    {
        "id": 720687,
        "name": "Füzesabony",
        "coord": {
            "lon": 20.41667,
            "lat": 47.75
        }
    },
    {
        "id": 720776,
        "name": "Földes",
        "coord": {
            "lon": 21.366671,
            "lat": 47.299999
        }
    },
    {
        "id": 720780,
        "name": "Földeák",
        "coord": {
            "lon": 20.5,
            "lat": 46.316669
        }
    },
    {
        "id": 720846,
        "name": "Fényeslitke",
        "coord": {
            "lon": 22.10009,
            "lat": 48.271332
        }
    },
    {
        "id": 720857,
        "name": "Felsőzsolca",
        "coord": {
            "lon": 20.866671,
            "lat": 48.099998
        }
    },
    {
        "id": 720862,
        "name": "Felsőtárkány",
        "coord": {
            "lon": 20.41667,
            "lat": 47.966671
        }
    },
    {
        "id": 720907,
        "name": "Felsőbánya",
        "coord": {
            "lon": 20.716669,
            "lat": 48.216671
        }
    },
    {
        "id": 720988,
        "name": "Fehérgyarmat",
        "coord": {
            "lon": 22.51667,
            "lat": 47.98333
        }
    },
    {
        "id": 720992,
        "name": "Fegyvernek",
        "coord": {
            "lon": 20.533331,
            "lat": 47.26667
        }
    },
    {
        "id": 721022,
        "name": "Farkaslyuk",
        "coord": {
            "lon": 20.316669,
            "lat": 48.183331
        }
    },
    {
        "id": 721024,
        "name": "Farkashegyi Szőlő",
        "coord": {
            "lon": 21.799999,
            "lat": 48.066669
        }
    },
    {
        "id": 721068,
        "name": "Fábiánsebestyén",
        "coord": {
            "lon": 20.466669,
            "lat": 46.683331
        }
    },
    {
        "id": 721119,
        "name": "Erdőtelek",
        "coord": {
            "lon": 20.316669,
            "lat": 47.683331
        }
    },
    {
        "id": 721158,
        "name": "Encsencs",
        "coord": {
            "lon": 22.116671,
            "lat": 47.73333
        }
    },
    {
        "id": 721159,
        "name": "Encs",
        "coord": {
            "lon": 21.133329,
            "lat": 48.333328
        }
    },
    {
        "id": 721161,
        "name": "Emőd",
        "coord": {
            "lon": 20.816669,
            "lat": 47.933331
        }
    },
    {
        "id": 721189,
        "name": "Elek",
        "coord": {
            "lon": 21.25,
            "lat": 46.533329
        }
    },
    {
        "id": 721208,
        "name": "Egyek",
        "coord": {
            "lon": 20.9,
            "lat": 47.633331
        }
    },
    {
        "id": 721225,
        "name": "Egerszalók",
        "coord": {
            "lon": 20.33333,
            "lat": 47.866669
        }
    },
    {
        "id": 721239,
        "name": "Eger",
        "coord": {
            "lon": 20.373289,
            "lat": 47.902649
        }
    },
    {
        "id": 721246,
        "name": "Edelény",
        "coord": {
            "lon": 20.73333,
            "lat": 48.299999
        }
    },
    {
        "id": 721257,
        "name": "Ebes",
        "coord": {
            "lon": 21.5,
            "lat": 47.466671
        }
    },
    {
        "id": 721304,
        "name": "Domoszló",
        "coord": {
            "lon": 20.116671,
            "lat": 47.833328
        }
    },
    {
        "id": 721305,
        "name": "Domonicstanya",
        "coord": {
            "lon": 21.65,
            "lat": 48.099998
        }
    },
    {
        "id": 721309,
        "name": "Dombrád",
        "coord": {
            "lon": 21.933331,
            "lat": 48.23333
        }
    },
    {
        "id": 721312,
        "name": "Dombiratos",
        "coord": {
            "lon": 21.116671,
            "lat": 46.416672
        }
    },
    {
        "id": 721314,
        "name": "Dombegyház",
        "coord": {
            "lon": 21.133329,
            "lat": 46.333328
        }
    },
    {
        "id": 721315,
        "name": "Dombár",
        "coord": {
            "lon": 21.933331,
            "lat": 48.23333
        }
    },
    {
        "id": 721316,
        "name": "Domaszék",
        "coord": {
            "lon": 20.01111,
            "lat": 46.249168
        }
    },
    {
        "id": 721338,
        "name": "Döge",
        "coord": {
            "lon": 22.063391,
            "lat": 48.262459
        }
    },
    {
        "id": 721349,
        "name": "Doboz",
        "coord": {
            "lon": 21.25,
            "lat": 46.73333
        }
    },
    {
        "id": 721404,
        "name": "Dévaványa",
        "coord": {
            "lon": 20.966669,
            "lat": 47.033329
        }
    },
    {
        "id": 721411,
        "name": "Deszk",
        "coord": {
            "lon": 20.243219,
            "lat": 46.218021
        }
    },
    {
        "id": 721418,
        "name": "Derzsicsere",
        "coord": {
            "lon": 22.133329,
            "lat": 47.833328
        }
    },
    {
        "id": 721428,
        "name": "Derecske",
        "coord": {
            "lon": 21.566669,
            "lat": 47.349998
        }
    },
    {
        "id": 721444,
        "name": "Demecser",
        "coord": {
            "lon": 21.91749,
            "lat": 48.11108
        }
    },
    {
        "id": 721472,
        "name": "Debrecen",
        "coord": {
            "lon": 21.633329,
            "lat": 47.533329
        }
    },
    {
        "id": 721559,
        "name": "Csorvás",
        "coord": {
            "lon": 20.83333,
            "lat": 46.633331
        }
    },
    {
        "id": 721575,
        "name": "Csontoskert",
        "coord": {
            "lon": 21.5,
            "lat": 47.816669
        }
    },
    {
        "id": 721589,
        "name": "Csongrád megye",
        "coord": {
            "lon": 20.25,
            "lat": 46.416672
        }
    },
    {
        "id": 721592,
        "name": "Csongrád",
        "coord": {
            "lon": 20.14242,
            "lat": 46.713322
        }
    },
    {
        "id": 721611,
        "name": "Csökmő",
        "coord": {
            "lon": 21.299999,
            "lat": 47.033329
        }
    },
    {
        "id": 721660,
        "name": "Csikóslapostanya",
        "coord": {
            "lon": 21.933331,
            "lat": 47.866669
        }
    },
    {
        "id": 721712,
        "name": "Cserkeszőlő",
        "coord": {
            "lon": 20.18701,
            "lat": 46.863201
        }
    },
    {
        "id": 721789,
        "name": "Csenger",
        "coord": {
            "lon": 22.68096,
            "lat": 47.835819
        }
    },
    {
        "id": 721812,
        "name": "Cseberér",
        "coord": {
            "lon": 20.23333,
            "lat": 47.133331
        }
    },
    {
        "id": 721856,
        "name": "Csanytelek",
        "coord": {
            "lon": 20.123421,
            "lat": 46.595001
        }
    },
    {
        "id": 721861,
        "name": "Csanádpalota",
        "coord": {
            "lon": 20.73333,
            "lat": 46.25
        }
    },
    {
        "id": 721864,
        "name": "Csanádapáca",
        "coord": {
            "lon": 20.883329,
            "lat": 46.549999
        }
    },
    {
        "id": 721913,
        "name": "Cigánd",
        "coord": {
            "lon": 21.891951,
            "lat": 48.255611
        }
    },
    {
        "id": 721923,
        "name": "Cibakháza",
        "coord": {
            "lon": 20.197531,
            "lat": 46.959759
        }
    },
    {
        "id": 721986,
        "name": "Buj",
        "coord": {
            "lon": 21.65,
            "lat": 48.099998
        }
    },
    {
        "id": 722011,
        "name": "Bucsa",
        "coord": {
            "lon": 21.0,
            "lat": 47.200001
        }
    },
    {
        "id": 722057,
        "name": "Borsodnádasd",
        "coord": {
            "lon": 20.25,
            "lat": 48.116669
        }
    },
    {
        "id": 722064,
        "name": "Borsod-Abauj Zemplen county",
        "coord": {
            "lon": 21.0,
            "lat": 48.25
        }
    },
    {
        "id": 722095,
        "name": "Borbélytanya",
        "coord": {
            "lon": 21.383329,
            "lat": 48.01667
        }
    },
    {
        "id": 722118,
        "name": "Boldva",
        "coord": {
            "lon": 20.799999,
            "lat": 48.216671
        }
    },
    {
        "id": 722133,
        "name": "Bököny",
        "coord": {
            "lon": 21.75,
            "lat": 47.73333
        }
    },
    {
        "id": 722171,
        "name": "Bogács",
        "coord": {
            "lon": 20.533331,
            "lat": 47.900002
        }
    },
    {
        "id": 722214,
        "name": "Bodahegyes",
        "coord": {
            "lon": 21.51667,
            "lat": 47.666672
        }
    },
    {
        "id": 722223,
        "name": "Bőcs",
        "coord": {
            "lon": 20.966669,
            "lat": 48.049999
        }
    },
    {
        "id": 722278,
        "name": "Biharnagybajom",
        "coord": {
            "lon": 21.23333,
            "lat": 47.216671
        }
    },
    {
        "id": 722280,
        "name": "Biharkeresztesi Tanyák",
        "coord": {
            "lon": 21.716669,
            "lat": 47.133331
        }
    },
    {
        "id": 722281,
        "name": "Biharkeresztes",
        "coord": {
            "lon": 21.716669,
            "lat": 47.133331
        }
    },
    {
        "id": 722300,
        "name": "Besenyszög",
        "coord": {
            "lon": 20.26667,
            "lat": 47.299999
        }
    },
    {
        "id": 722301,
        "name": "Besenyőtelek",
        "coord": {
            "lon": 20.433331,
            "lat": 47.700001
        }
    },
    {
        "id": 722324,
        "name": "Berettyóújfalu",
        "coord": {
            "lon": 21.549999,
            "lat": 47.216671
        }
    },
    {
        "id": 722406,
        "name": "Bellegelő",
        "coord": {
            "lon": 21.633329,
            "lat": 47.48333
        }
    },
    {
        "id": 722424,
        "name": "Bélapátfalva",
        "coord": {
            "lon": 20.366671,
            "lat": 48.049999
        }
    },
    {
        "id": 722431,
        "name": "Békésszentandrás",
        "coord": {
            "lon": 20.48333,
            "lat": 46.866669
        }
    },
    {
        "id": 722432,
        "name": "Békéssámson",
        "coord": {
            "lon": 20.633329,
            "lat": 46.416672
        }
    },
    {
        "id": 722433,
        "name": "Bekes County",
        "coord": {
            "lon": 21.0,
            "lat": 46.75
        }
    },
    {
        "id": 722437,
        "name": "Békéscsaba",
        "coord": {
            "lon": 21.1,
            "lat": 46.683331
        }
    },
    {
        "id": 722439,
        "name": "Békés",
        "coord": {
            "lon": 21.133329,
            "lat": 46.76667
        }
    },
    {
        "id": 722448,
        "name": "Bekecs",
        "coord": {
            "lon": 21.183331,
            "lat": 48.150002
        }
    },
    {
        "id": 722471,
        "name": "Baytanya",
        "coord": {
            "lon": 22.200001,
            "lat": 48.066669
        }
    },
    {
        "id": 722477,
        "name": "Battonya",
        "coord": {
            "lon": 21.01667,
            "lat": 46.283329
        }
    },
    {
        "id": 722491,
        "name": "Báthoritanya",
        "coord": {
            "lon": 22.066669,
            "lat": 47.799999
        }
    },
    {
        "id": 722494,
        "name": "Bata",
        "coord": {
            "lon": 20.6,
            "lat": 47.466671
        }
    },
    {
        "id": 722555,
        "name": "Báránd",
        "coord": {
            "lon": 21.23333,
            "lat": 47.299999
        }
    },
    {
        "id": 722636,
        "name": "Balmazújváros",
        "coord": {
            "lon": 21.35,
            "lat": 47.616669
        }
    },
    {
        "id": 722650,
        "name": "Balkány",
        "coord": {
            "lon": 21.86314,
            "lat": 47.77066
        }
    },
    {
        "id": 722676,
        "name": "Balástya",
        "coord": {
            "lon": 20.00816,
            "lat": 46.422771
        }
    },
    {
        "id": 722682,
        "name": "Baktalórántháza",
        "coord": {
            "lon": 22.08333,
            "lat": 48.0
        }
    },
    {
        "id": 722691,
        "name": "Baks",
        "coord": {
            "lon": 20.102131,
            "lat": 46.542969
        }
    },
    {
        "id": 722738,
        "name": "Bagamér",
        "coord": {
            "lon": 21.989,
            "lat": 47.448818
        }
    },
    {
        "id": 722775,
        "name": "Aszaló",
        "coord": {
            "lon": 20.966669,
            "lat": 48.216671
        }
    },
    {
        "id": 722802,
        "name": "Arnót",
        "coord": {
            "lon": 20.85832,
            "lat": 48.130581
        }
    },
    {
        "id": 722803,
        "name": "Arló",
        "coord": {
            "lon": 20.26667,
            "lat": 48.183331
        }
    },
    {
        "id": 722823,
        "name": "Aranyosapáti",
        "coord": {
            "lon": 22.259159,
            "lat": 48.205952
        }
    },
    {
        "id": 722832,
        "name": "Aranyág",
        "coord": {
            "lon": 21.283331,
            "lat": 46.650002
        }
    },
    {
        "id": 722849,
        "name": "Apátfalva",
        "coord": {
            "lon": 20.58333,
            "lat": 46.166672
        }
    },
    {
        "id": 722852,
        "name": "Apagy",
        "coord": {
            "lon": 21.93539,
            "lat": 47.964298
        }
    },
    {
        "id": 722866,
        "name": "Andrástanya",
        "coord": {
            "lon": 20.73333,
            "lat": 48.400002
        }
    },
    {
        "id": 722872,
        "name": "Andornaktálya",
        "coord": {
            "lon": 20.41667,
            "lat": 47.849998
        }
    },
    {
        "id": 722876,
        "name": "Anarcs",
        "coord": {
            "lon": 22.11167,
            "lat": 48.176418
        }
    },
    {
        "id": 722882,
        "name": "Alsózsolca",
        "coord": {
            "lon": 20.880461,
            "lat": 48.06982
        }
    },
    {
        "id": 722884,
        "name": "Alsóváros",
        "coord": {
            "lon": 20.133329,
            "lat": 46.23333
        }
    },
    {
        "id": 722906,
        "name": "Alsórét",
        "coord": {
            "lon": 20.24782,
            "lat": 46.66568
        }
    },
    {
        "id": 722973,
        "name": "Alattyán",
        "coord": {
            "lon": 20.0422,
            "lat": 47.427052
        }
    },
    {
        "id": 722998,
        "name": "Ajak",
        "coord": {
            "lon": 22.062731,
            "lat": 48.17664
        }
    },
    {
        "id": 723030,
        "name": "Abony",
        "coord": {
            "lon": 20.004761,
            "lat": 47.189899
        }
    },
    {
        "id": 723036,
        "name": "Abaújszántó",
        "coord": {
            "lon": 21.200001,
            "lat": 48.283329
        }
    },
    {
        "id": 723041,
        "name": "Abasár",
        "coord": {
            "lon": 20.003241,
            "lat": 47.79705
        }
    },
    {
        "id": 723044,
        "name": "Abádszalók",
        "coord": {
            "lon": 20.6,
            "lat": 47.466671
        }
    },
    {
        "id": 3042441,
        "name": "Zsombó",
        "coord": {
            "lon": 19.97464,
            "lat": 46.325661
        }
    },
    {
        "id": 3042504,
        "name": "Zsámbok",
        "coord": {
            "lon": 19.610479,
            "lat": 47.543812
        }
    },
    {
        "id": 3042507,
        "name": "Zsámbék",
        "coord": {
            "lon": 18.72011,
            "lat": 47.548141
        }
    },
    {
        "id": 3042516,
        "name": "Zomba",
        "coord": {
            "lon": 18.565769,
            "lat": 46.410839
        }
    },
    {
        "id": 3042520,
        "name": "Zöldmalom",
        "coord": {
            "lon": 18.033331,
            "lat": 47.5
        }
    },
    {
        "id": 3042536,
        "name": "Zirc",
        "coord": {
            "lon": 17.87373,
            "lat": 47.263618
        }
    },
    {
        "id": 3042569,
        "name": "Zánka",
        "coord": {
            "lon": 17.684731,
            "lat": 46.87146
        }
    },
    {
        "id": 3042575,
        "name": "Zámoly",
        "coord": {
            "lon": 18.4081,
            "lat": 47.316669
        }
    },
    {
        "id": 3042580,
        "name": "Zamárdi",
        "coord": {
            "lon": 17.953659,
            "lat": 46.88488
        }
    },
    {
        "id": 3042583,
        "name": "Zalavár",
        "coord": {
            "lon": 17.156019,
            "lat": 46.6684
        }
    },
    {
        "id": 3042603,
        "name": "Zalaszentgrót",
        "coord": {
            "lon": 17.07925,
            "lat": 46.94474
        }
    },
    {
        "id": 3042610,
        "name": "Zalasárszeg",
        "coord": {
            "lon": 17.08053,
            "lat": 46.494331
        }
    },
    {
        "id": 3042613,
        "name": "Zala megye",
        "coord": {
            "lon": 16.83333,
            "lat": 46.666672
        }
    },
    {
        "id": 3042617,
        "name": "Zalalövő",
        "coord": {
            "lon": 16.5875,
            "lat": 46.848019
        }
    },
    {
        "id": 3042621,
        "name": "Zalakomár",
        "coord": {
            "lon": 17.180941,
            "lat": 46.53796
        }
    },
    {
        "id": 3042622,
        "name": "Zalakarosi Puszta",
        "coord": {
            "lon": 17.133329,
            "lat": 46.566669
        }
    },
    {
        "id": 3042638,
        "name": "Zalaegerszeg",
        "coord": {
            "lon": 16.843889,
            "lat": 46.84
        }
    },
    {
        "id": 3042652,
        "name": "Zákányszék",
        "coord": {
            "lon": 19.88975,
            "lat": 46.274529
        }
    },
    {
        "id": 3042657,
        "name": "Zajk",
        "coord": {
            "lon": 16.71871,
            "lat": 46.485699
        }
    },
    {
        "id": 3042661,
        "name": "Zagyvaszántó",
        "coord": {
            "lon": 19.670919,
            "lat": 47.777031
        }
    },
    {
        "id": 3042685,
        "name": "Webertanya",
        "coord": {
            "lon": 19.23333,
            "lat": 47.383331
        }
    },
    {
        "id": 3042746,
        "name": "Vonyarcvashegy",
        "coord": {
            "lon": 17.31172,
            "lat": 46.75742
        }
    },
    {
        "id": 3042824,
        "name": "Visegrád",
        "coord": {
            "lon": 18.9709,
            "lat": 47.785259
        }
    },
    {
        "id": 3042840,
        "name": "Virághegy",
        "coord": {
            "lon": 19.35,
            "lat": 47.48333
        }
    },
    {
        "id": 3042841,
        "name": "Violatag",
        "coord": {
            "lon": 17.51667,
            "lat": 47.51667
        }
    },
    {
        "id": 3042871,
        "name": "Villány",
        "coord": {
            "lon": 18.45389,
            "lat": 45.868889
        }
    },
    {
        "id": 3042872,
        "name": "Villa",
        "coord": {
            "lon": 18.633329,
            "lat": 47.48333
        }
    },
    {
        "id": 3042925,
        "name": "Veszprém megye",
        "coord": {
            "lon": 17.66667,
            "lat": 47.166672
        }
    },
    {
        "id": 3042929,
        "name": "Veszprém",
        "coord": {
            "lon": 17.911489,
            "lat": 47.093269
        }
    },
    {
        "id": 3042941,
        "name": "Vértesszőlős",
        "coord": {
            "lon": 18.3813,
            "lat": 47.621059
        }
    },
    {
        "id": 3042956,
        "name": "Verőce",
        "coord": {
            "lon": 19.03484,
            "lat": 47.82468
        }
    },
    {
        "id": 3042968,
        "name": "Veresegyház",
        "coord": {
            "lon": 19.295361,
            "lat": 47.645901
        }
    },
    {
        "id": 3042974,
        "name": "Verdungos",
        "coord": {
            "lon": 19.75,
            "lat": 47.416672
        }
    },
    {
        "id": 3042977,
        "name": "Vép",
        "coord": {
            "lon": 16.722481,
            "lat": 47.230412
        }
    },
    {
        "id": 3042998,
        "name": "Velence",
        "coord": {
            "lon": 18.65484,
            "lat": 47.238548
        }
    },
    {
        "id": 3043019,
        "name": "Vecsés",
        "coord": {
            "lon": 19.28648,
            "lat": 47.407051
        }
    },
    {
        "id": 3043033,
        "name": "Vasvár",
        "coord": {
            "lon": 16.79954,
            "lat": 47.049278
        }
    },
    {
        "id": 3043034,
        "name": "Vasúti Újlaktanya",
        "coord": {
            "lon": 16.549999,
            "lat": 47.383331
        }
    },
    {
        "id": 3043047,
        "name": "Vas megye",
        "coord": {
            "lon": 16.75,
            "lat": 47.166672
        }
    },
    {
        "id": 3043048,
        "name": "Vaskút",
        "coord": {
            "lon": 18.98514,
            "lat": 46.107819
        }
    },
    {
        "id": 3043095,
        "name": "Várpalota",
        "coord": {
            "lon": 18.13954,
            "lat": 47.19936
        }
    },
    {
        "id": 3043096,
        "name": "Városrétidűlő",
        "coord": {
            "lon": 18.933331,
            "lat": 46.98333
        }
    },
    {
        "id": 3043111,
        "name": "Városföld",
        "coord": {
            "lon": 19.75668,
            "lat": 46.816738
        }
    },
    {
        "id": 3043117,
        "name": "Várkonyitanya",
        "coord": {
            "lon": 18.98333,
            "lat": 46.916672
        }
    },
    {
        "id": 3043124,
        "name": "Varjúdűlő",
        "coord": {
            "lon": 18.41667,
            "lat": 47.200001
        }
    },
    {
        "id": 3043210,
        "name": "Vámosgyörk",
        "coord": {
            "lon": 19.929239,
            "lat": 47.68428
        }
    },
    {
        "id": 3043217,
        "name": "Valkó",
        "coord": {
            "lon": 19.512671,
            "lat": 47.563911
        }
    },
    {
        "id": 3043224,
        "name": "Vál",
        "coord": {
            "lon": 18.67931,
            "lat": 47.36264
        }
    },
    {
        "id": 3043233,
        "name": "Vajszló",
        "coord": {
            "lon": 17.98406,
            "lat": 45.859581
        }
    },
    {
        "id": 3043238,
        "name": "Vajdamajor",
        "coord": {
            "lon": 16.966669,
            "lat": 46.51667
        }
    },
    {
        "id": 3043278,
        "name": "Vácszentlászló",
        "coord": {
            "lon": 19.53771,
            "lat": 47.574001
        }
    },
    {
        "id": 3043293,
        "name": "Vác",
        "coord": {
            "lon": 19.13612,
            "lat": 47.775909
        }
    },
    {
        "id": 3043306,
        "name": "Úsztatómajor",
        "coord": {
            "lon": 17.25,
            "lat": 46.76667
        }
    },
    {
        "id": 3043317,
        "name": "Üröm",
        "coord": {
            "lon": 19.015829,
            "lat": 47.596741
        }
    },
    {
        "id": 3043318,
        "name": "Úrkút",
        "coord": {
            "lon": 17.64393,
            "lat": 47.085049
        }
    },
    {
        "id": 3043321,
        "name": "Úri",
        "coord": {
            "lon": 19.52762,
            "lat": 47.414291
        }
    },
    {
        "id": 3043345,
        "name": "Uradalmi Szőllőtelep",
        "coord": {
            "lon": 18.316669,
            "lat": 47.650002
        }
    },
    {
        "id": 3043356,
        "name": "Üllő",
        "coord": {
            "lon": 19.35533,
            "lat": 47.387711
        }
    },
    {
        "id": 3043358,
        "name": "Üllés",
        "coord": {
            "lon": 19.84454,
            "lat": 46.336109
        }
    },
    {
        "id": 3043417,
        "name": "Újtelep",
        "coord": {
            "lon": 17.08333,
            "lat": 46.950001
        }
    },
    {
        "id": 3043448,
        "name": "Újszilvás",
        "coord": {
            "lon": 19.92477,
            "lat": 47.274769
        }
    },
    {
        "id": 3043542,
        "name": "Újhegy",
        "coord": {
            "lon": 17.283331,
            "lat": 46.98333
        }
    },
    {
        "id": 3043581,
        "name": "Újhartyán",
        "coord": {
            "lon": 19.386379,
            "lat": 47.21981
        }
    },
    {
        "id": 3043627,
        "name": "Tyúkosdűlő",
        "coord": {
            "lon": 19.08333,
            "lat": 47.666672
        }
    },
    {
        "id": 3043634,
        "name": "Tüzérlaktanya",
        "coord": {
            "lon": 17.466669,
            "lat": 47.333328
        }
    },
    {
        "id": 3043671,
        "name": "Türje",
        "coord": {
            "lon": 17.10742,
            "lat": 46.983662
        }
    },
    {
        "id": 3043674,
        "name": "Turbina",
        "coord": {
            "lon": 16.716669,
            "lat": 47.400002
        }
    },
    {
        "id": 3043678,
        "name": "Tura",
        "coord": {
            "lon": 19.602791,
            "lat": 47.609241
        }
    },
    {
        "id": 3043685,
        "name": "Túlatarna",
        "coord": {
            "lon": 19.91667,
            "lat": 47.5
        }
    },
    {
        "id": 3043700,
        "name": "Trombitásmalom",
        "coord": {
            "lon": 16.949181,
            "lat": 47.27306
        }
    },
    {
        "id": 3043736,
        "name": "Tóthtanya",
        "coord": {
            "lon": 19.35,
            "lat": 47.383331
        }
    },
    {
        "id": 3043758,
        "name": "Törtel",
        "coord": {
            "lon": 19.93714,
            "lat": 47.122089
        }
    },
    {
        "id": 3043778,
        "name": "Törökbálint",
        "coord": {
            "lon": 18.913561,
            "lat": 47.42931
        }
    },
    {
        "id": 3043803,
        "name": "Torbágyi Tanyák",
        "coord": {
            "lon": 18.816669,
            "lat": 47.466671
        }
    },
    {
        "id": 3043820,
        "name": "Tompa",
        "coord": {
            "lon": 19.539101,
            "lat": 46.206051
        }
    },
    {
        "id": 3043832,
        "name": "Töltéstava",
        "coord": {
            "lon": 17.733761,
            "lat": 47.626091
        }
    },
    {
        "id": 3043845,
        "name": "Tolna megye",
        "coord": {
            "lon": 18.58333,
            "lat": 46.5
        }
    },
    {
        "id": 3043849,
        "name": "Tolna",
        "coord": {
            "lon": 18.78248,
            "lat": 46.426762
        }
    },
    {
        "id": 3043860,
        "name": "Tököl",
        "coord": {
            "lon": 18.96249,
            "lat": 47.321781
        }
    },
    {
        "id": 3043865,
        "name": "Tokod",
        "coord": {
            "lon": 18.65889,
            "lat": 47.722771
        }
    },
    {
        "id": 3043887,
        "name": "Tóalmás",
        "coord": {
            "lon": 19.666571,
            "lat": 47.50782
        }
    },
    {
        "id": 3043889,
        "name": "Tizenháromváros",
        "coord": {
            "lon": 16.933331,
            "lat": 47.25
        }
    },
    {
        "id": 3043895,
        "name": "Tisztviselőtelep",
        "coord": {
            "lon": 18.01667,
            "lat": 47.033329
        }
    },
    {
        "id": 3043927,
        "name": "Tihany",
        "coord": {
            "lon": 17.889179,
            "lat": 46.913689
        }
    },
    {
        "id": 3043946,
        "name": "Tét",
        "coord": {
            "lon": 17.50802,
            "lat": 47.519218
        }
    },
    {
        "id": 3043984,
        "name": "Tengelic",
        "coord": {
            "lon": 18.71117,
            "lat": 46.528782
        }
    },
    {
        "id": 3044001,
        "name": "Temetődűlő",
        "coord": {
            "lon": 17.799999,
            "lat": 46.049999
        }
    },
    {
        "id": 3044004,
        "name": "Telki",
        "coord": {
            "lon": 18.828159,
            "lat": 47.547901
        }
    },
    {
        "id": 3044062,
        "name": "Tázlár",
        "coord": {
            "lon": 19.51436,
            "lat": 46.548241
        }
    },
    {
        "id": 3044082,
        "name": "Tatabánya",
        "coord": {
            "lon": 18.39325,
            "lat": 47.584942
        }
    },
    {
        "id": 3044083,
        "name": "Tata",
        "coord": {
            "lon": 18.31838,
            "lat": 47.652889
        }
    },
    {
        "id": 3044084,
        "name": "Tát",
        "coord": {
            "lon": 18.64813,
            "lat": 47.741482
        }
    },
    {
        "id": 3044088,
        "name": "Taszár",
        "coord": {
            "lon": 17.905939,
            "lat": 46.374668
        }
    },
    {
        "id": 3044093,
        "name": "Tass",
        "coord": {
            "lon": 19.029881,
            "lat": 47.02095
        }
    },
    {
        "id": 3044103,
        "name": "Tárnok",
        "coord": {
            "lon": 18.845791,
            "lat": 47.373268
        }
    },
    {
        "id": 3044119,
        "name": "Tarján",
        "coord": {
            "lon": 18.5107,
            "lat": 47.61042
        }
    },
    {
        "id": 3044122,
        "name": "Tardos",
        "coord": {
            "lon": 18.44416,
            "lat": 47.661919
        }
    },
    {
        "id": 3044131,
        "name": "Tar",
        "coord": {
            "lon": 19.746099,
            "lat": 47.953709
        }
    },
    {
        "id": 3044141,
        "name": "Tapolca",
        "coord": {
            "lon": 17.44117,
            "lat": 46.881519
        }
    },
    {
        "id": 3044144,
        "name": "Táplánszentkereszt",
        "coord": {
            "lon": 16.696131,
            "lat": 47.194962
        }
    },
    {
        "id": 3044147,
        "name": "Tápiószőlős",
        "coord": {
            "lon": 19.85133,
            "lat": 47.302479
        }
    },
    {
        "id": 3044149,
        "name": "Tápiószentmárton",
        "coord": {
            "lon": 19.746481,
            "lat": 47.339901
        }
    },
    {
        "id": 3044151,
        "name": "Tápiószele",
        "coord": {
            "lon": 19.877239,
            "lat": 47.33609
        }
    },
    {
        "id": 3044153,
        "name": "Tápiószecső",
        "coord": {
            "lon": 19.60923,
            "lat": 47.450001
        }
    },
    {
        "id": 3044154,
        "name": "Sülysáp",
        "coord": {
            "lon": 19.533689,
            "lat": 47.452061
        }
    },
    {
        "id": 3044156,
        "name": "Tápióság",
        "coord": {
            "lon": 19.63047,
            "lat": 47.402
        }
    },
    {
        "id": 3044160,
        "name": "Tápiógyörgye",
        "coord": {
            "lon": 19.952761,
            "lat": 47.335049
        }
    },
    {
        "id": 3044161,
        "name": "Tápióbicske",
        "coord": {
            "lon": 19.68609,
            "lat": 47.360958
        }
    },
    {
        "id": 3044171,
        "name": "Tamási",
        "coord": {
            "lon": 18.283331,
            "lat": 46.633331
        }
    },
    {
        "id": 3044181,
        "name": "Taksony",
        "coord": {
            "lon": 19.06694,
            "lat": 47.329681
        }
    },
    {
        "id": 3044191,
        "name": "Tahitótfalu",
        "coord": {
            "lon": 19.1,
            "lat": 47.75
        }
    },
    {
        "id": 3044195,
        "name": "Tagosztály",
        "coord": {
            "lon": 18.950001,
            "lat": 47.166672
        }
    },
    {
        "id": 3044211,
        "name": "Táborfalva",
        "coord": {
            "lon": 19.478371,
            "lat": 47.099419
        }
    },
    {
        "id": 3044221,
        "name": "Tab",
        "coord": {
            "lon": 18.032009,
            "lat": 46.73135
        }
    },
    {
        "id": 3044227,
        "name": "Szurdokpüspöki",
        "coord": {
            "lon": 19.692181,
            "lat": 47.85923
        }
    },
    {
        "id": 3044254,
        "name": "Szuhahuta",
        "coord": {
            "lon": 19.91667,
            "lat": 47.933331
        }
    },
    {
        "id": 3044305,
        "name": "Szomód",
        "coord": {
            "lon": 18.340611,
            "lat": 47.684921
        }
    },
    {
        "id": 3044310,
        "name": "Szombathely",
        "coord": {
            "lon": 16.62155,
            "lat": 47.230881
        }
    },
    {
        "id": 3044330,
        "name": "Szolnokihegy",
        "coord": {
            "lon": 19.72751,
            "lat": 46.898079
        }
    },
    {
        "id": 3044370,
        "name": "Sződliget",
        "coord": {
            "lon": 19.14749,
            "lat": 47.73259
        }
    },
    {
        "id": 3044371,
        "name": "Sződ",
        "coord": {
            "lon": 19.17046,
            "lat": 47.724388
        }
    },
    {
        "id": 3044380,
        "name": "Szob",
        "coord": {
            "lon": 18.870199,
            "lat": 47.81921
        }
    },
    {
        "id": 3044469,
        "name": "Szigetvár",
        "coord": {
            "lon": 17.80554,
            "lat": 46.048649
        }
    },
    {
        "id": 3044470,
        "name": "Szigetújfalu",
        "coord": {
            "lon": 18.92746,
            "lat": 47.234169
        }
    },
    {
        "id": 3044475,
        "name": "Szigetszentmiklós",
        "coord": {
            "lon": 19.04335,
            "lat": 47.343819
        }
    },
    {
        "id": 3044487,
        "name": "Szigethalom",
        "coord": {
            "lon": 19.002621,
            "lat": 47.322281
        }
    },
    {
        "id": 3044492,
        "name": "Szigetcsép",
        "coord": {
            "lon": 18.97048,
            "lat": 47.264919
        }
    },
    {
        "id": 3044495,
        "name": "Sziget",
        "coord": {
            "lon": 18.783331,
            "lat": 46.433331
        }
    },
    {
        "id": 3044567,
        "name": "Szentmártonkáta",
        "coord": {
            "lon": 19.701429,
            "lat": 47.454189
        }
    },
    {
        "id": 3044574,
        "name": "Szentlőrinckáta",
        "coord": {
            "lon": 19.752859,
            "lat": 47.51947
        }
    },
    {
        "id": 3044575,
        "name": "Szentlőrinc",
        "coord": {
            "lon": 17.98719,
            "lat": 46.040161
        }
    },
    {
        "id": 3044600,
        "name": "Szentkirályszabadja",
        "coord": {
            "lon": 17.97052,
            "lat": 47.057598
        }
    },
    {
        "id": 3044607,
        "name": "Szentkirály",
        "coord": {
            "lon": 19.918461,
            "lat": 46.918919
        }
    },
    {
        "id": 3044615,
        "name": "Szentjánoskúti Dűlő",
        "coord": {
            "lon": 18.75,
            "lat": 47.799999
        }
    },
    {
        "id": 3044669,
        "name": "Szentgotthárd",
        "coord": {
            "lon": 16.273581,
            "lat": 46.95261
        }
    },
    {
        "id": 3044671,
        "name": "Szentgálpuszta",
        "coord": {
            "lon": 18.626011,
            "lat": 46.37479
        }
    },
    {
        "id": 3044681,
        "name": "Szentendre",
        "coord": {
            "lon": 19.075609,
            "lat": 47.66943
        }
    },
    {
        "id": 3044760,
        "name": "Szekszárd",
        "coord": {
            "lon": 18.709049,
            "lat": 46.35014
        }
    },
    {
        "id": 3044774,
        "name": "Székesfehérvár",
        "coord": {
            "lon": 18.410339,
            "lat": 47.189949
        }
    },
    {
        "id": 3044786,
        "name": "Székelytelep",
        "coord": {
            "lon": 19.700001,
            "lat": 46.900002
        }
    },
    {
        "id": 3044800,
        "name": "Szedres",
        "coord": {
            "lon": 18.68305,
            "lat": 46.47551
        }
    },
    {
        "id": 3044809,
        "name": "Szécsény",
        "coord": {
            "lon": 19.520189,
            "lat": 48.08057
        }
    },
    {
        "id": 3044821,
        "name": "Százhalombatta",
        "coord": {
            "lon": 18.921419,
            "lat": 47.32579
        }
    },
    {
        "id": 3044835,
        "name": "Szászvár",
        "coord": {
            "lon": 18.37566,
            "lat": 46.27673
        }
    },
    {
        "id": 3044861,
        "name": "Szárliget",
        "coord": {
            "lon": 18.494801,
            "lat": 47.515808
        }
    },
    {
        "id": 3044909,
        "name": "Szany",
        "coord": {
            "lon": 17.30402,
            "lat": 47.46423
        }
    },
    {
        "id": 3044916,
        "name": "Szank",
        "coord": {
            "lon": 19.66103,
            "lat": 46.557129
        }
    },
    {
        "id": 3044941,
        "name": "Szalkszentmárton",
        "coord": {
            "lon": 19.01178,
            "lat": 46.975651
        }
    },
    {
        "id": 3044990,
        "name": "Szada",
        "coord": {
            "lon": 19.316669,
            "lat": 47.633331
        }
    },
    {
        "id": 3045017,
        "name": "Szabadszállás",
        "coord": {
            "lon": 19.22324,
            "lat": 46.875751
        }
    },
    {
        "id": 3045039,
        "name": "Szabadbattyán",
        "coord": {
            "lon": 18.36824,
            "lat": 47.119019
        }
    },
    {
        "id": 3045045,
        "name": "Süttő",
        "coord": {
            "lon": 18.44873,
            "lat": 47.758862
        }
    },
    {
        "id": 3045070,
        "name": "Sümeg",
        "coord": {
            "lon": 17.282089,
            "lat": 46.977032
        }
    },
    {
        "id": 3045076,
        "name": "Sükösd",
        "coord": {
            "lon": 18.995239,
            "lat": 46.281811
        }
    },
    {
        "id": 3045144,
        "name": "Sóskút",
        "coord": {
            "lon": 18.82247,
            "lat": 47.406651
        }
    },
    {
        "id": 3045190,
        "name": "Sopron",
        "coord": {
            "lon": 16.59049,
            "lat": 47.685009
        }
    },
    {
        "id": 3045192,
        "name": "Soponya",
        "coord": {
            "lon": 18.45343,
            "lat": 47.014851
        }
    },
    {
        "id": 3045201,
        "name": "Somoskőújfalu",
        "coord": {
            "lon": 19.82303,
            "lat": 48.163738
        }
    },
    {
        "id": 3045212,
        "name": "Somogyvár",
        "coord": {
            "lon": 17.662889,
            "lat": 46.581402
        }
    },
    {
        "id": 3045226,
        "name": "Somogy megye",
        "coord": {
            "lon": 17.58333,
            "lat": 46.416672
        }
    },
    {
        "id": 3045294,
        "name": "Solymár",
        "coord": {
            "lon": 18.932119,
            "lat": 47.592461
        }
    },
    {
        "id": 3045298,
        "name": "Soltvadkert",
        "coord": {
            "lon": 19.39389,
            "lat": 46.578892
        }
    },
    {
        "id": 3045303,
        "name": "Solt",
        "coord": {
            "lon": 18.999929,
            "lat": 46.800671
        }
    },
    {
        "id": 3045332,
        "name": "Siófok",
        "coord": {
            "lon": 18.058001,
            "lat": 46.904121
        }
    },
    {
        "id": 3045341,
        "name": "Simontornya",
        "coord": {
            "lon": 18.554899,
            "lat": 46.75462
        }
    },
    {
        "id": 3045364,
        "name": "Siklós",
        "coord": {
            "lon": 18.29752,
            "lat": 45.854988
        }
    },
    {
        "id": 3045386,
        "name": "Seregélyes",
        "coord": {
            "lon": 18.565001,
            "lat": 47.1105
        }
    },
    {
        "id": 3045404,
        "name": "Selyemgyártelep",
        "coord": {
            "lon": 18.549999,
            "lat": 47.76667
        }
    },
    {
        "id": 3045406,
        "name": "Sellye",
        "coord": {
            "lon": 17.847111,
            "lat": 45.872471
        }
    },
    {
        "id": 3045409,
        "name": "Sejce",
        "coord": {
            "lon": 19.133329,
            "lat": 47.783329
        }
    },
    {
        "id": 3045411,
        "name": "Segesd",
        "coord": {
            "lon": 17.35132,
            "lat": 46.341419
        }
    },
    {
        "id": 3045428,
        "name": "Scherctanya",
        "coord": {
            "lon": 18.91667,
            "lat": 47.433331
        }
    },
    {
        "id": 3045479,
        "name": "Sásd",
        "coord": {
            "lon": 18.107759,
            "lat": 46.255199
        }
    },
    {
        "id": 3045487,
        "name": "Sárvár",
        "coord": {
            "lon": 16.935249,
            "lat": 47.253948
        }
    },
    {
        "id": 3045493,
        "name": "Sárszentmihály",
        "coord": {
            "lon": 18.338791,
            "lat": 47.15321
        }
    },
    {
        "id": 3045512,
        "name": "Sárosd",
        "coord": {
            "lon": 18.64357,
            "lat": 47.042728
        }
    },
    {
        "id": 3045520,
        "name": "Sármellék",
        "coord": {
            "lon": 17.16865,
            "lat": 46.712212
        }
    },
    {
        "id": 3045530,
        "name": "Sárkeresztúr",
        "coord": {
            "lon": 18.543909,
            "lat": 47.005402
        }
    },
    {
        "id": 3045542,
        "name": "Sárisáp",
        "coord": {
            "lon": 18.679331,
            "lat": 47.676998
        }
    },
    {
        "id": 3045564,
        "name": "Sárbogárd",
        "coord": {
            "lon": 18.620411,
            "lat": 46.886921
        }
    },
    {
        "id": 3045581,
        "name": "Sándortanya",
        "coord": {
            "lon": 17.533331,
            "lat": 47.383331
        }
    },
    {
        "id": 3045643,
        "name": "Salgótarján",
        "coord": {
            "lon": 19.80303,
            "lat": 48.098721
        }
    },
    {
        "id": 3045688,
        "name": "Ruzsa",
        "coord": {
            "lon": 19.747141,
            "lat": 46.288059
        }
    },
    {
        "id": 3045704,
        "name": "Rózsaszentmárton",
        "coord": {
            "lon": 19.7421,
            "lat": 47.782001
        }
    },
    {
        "id": 3045744,
        "name": "Rőtivölgy",
        "coord": {
            "lon": 16.549999,
            "lat": 47.383331
        }
    },
    {
        "id": 3045758,
        "name": "Romhány",
        "coord": {
            "lon": 19.257231,
            "lat": 47.926182
        }
    },
    {
        "id": 3045804,
        "name": "Rimóc",
        "coord": {
            "lon": 19.5301,
            "lat": 48.036949
        }
    },
    {
        "id": 3045836,
        "name": "Révfülöp",
        "coord": {
            "lon": 17.619671,
            "lat": 46.825729
        }
    },
    {
        "id": 3045838,
        "name": "Revetekdűlő",
        "coord": {
            "lon": 19.283331,
            "lat": 47.650002
        }
    },
    {
        "id": 3045846,
        "name": "Rétság",
        "coord": {
            "lon": 19.137199,
            "lat": 47.928162
        }
    },
    {
        "id": 3045883,
        "name": "Répcelaki Tanya",
        "coord": {
            "lon": 17.01667,
            "lat": 47.416672
        }
    },
    {
        "id": 3045884,
        "name": "Répcelak",
        "coord": {
            "lon": 17.01795,
            "lat": 47.421051
        }
    },
    {
        "id": 3045926,
        "name": "Rédeymajor",
        "coord": {
            "lon": 17.15,
            "lat": 47.25
        }
    },
    {
        "id": 3045991,
        "name": "Rajka",
        "coord": {
            "lon": 17.19821,
            "lat": 47.996429
        }
    },
    {
        "id": 3046026,
        "name": "Ráckeve",
        "coord": {
            "lon": 18.94478,
            "lat": 47.16095
        }
    },
    {
        "id": 3046027,
        "name": "Ráckeresztúr",
        "coord": {
            "lon": 18.833429,
            "lat": 47.273602
        }
    },
    {
        "id": 3046036,
        "name": "Rácalmás",
        "coord": {
            "lon": 18.940559,
            "lat": 47.022629
        }
    },
    {
        "id": 3046039,
        "name": "Rabkertészet",
        "coord": {
            "lon": 17.91667,
            "lat": 47.099998
        }
    },
    {
        "id": 3046056,
        "name": "Rábapatona",
        "coord": {
            "lon": 17.48004,
            "lat": 47.63224
        }
    },
    {
        "id": 3046073,
        "name": "Pusztavám",
        "coord": {
            "lon": 18.22648,
            "lat": 47.429482
        }
    },
    {
        "id": 3046076,
        "name": "Pusztaszer",
        "coord": {
            "lon": 19.988239,
            "lat": 46.550831
        }
    },
    {
        "id": 3046087,
        "name": "Pusztaszabolcs",
        "coord": {
            "lon": 18.76704,
            "lat": 47.13718
        }
    },
    {
        "id": 3046110,
        "name": "Pusztagyimót Felső",
        "coord": {
            "lon": 17.51667,
            "lat": 47.366669
        }
    },
    {
        "id": 3046156,
        "name": "Priglitanya",
        "coord": {
            "lon": 17.283331,
            "lat": 47.866669
        }
    },
    {
        "id": 3046230,
        "name": "Pomáz",
        "coord": {
            "lon": 19.02784,
            "lat": 47.642269
        }
    },
    {
        "id": 3046249,
        "name": "Polgárdi",
        "coord": {
            "lon": 18.302,
            "lat": 47.060989
        }
    },
    {
        "id": 3046269,
        "name": "Pogány",
        "coord": {
            "lon": 18.26,
            "lat": 45.98
        }
    },
    {
        "id": 3046284,
        "name": "Pityerdűlő",
        "coord": {
            "lon": 18.41667,
            "lat": 47.200001
        }
    },
    {
        "id": 3046336,
        "name": "Pincehely",
        "coord": {
            "lon": 18.43935,
            "lat": 46.68095
        }
    },
    {
        "id": 3046340,
        "name": "Pilisvörösvár",
        "coord": {
            "lon": 18.90893,
            "lat": 47.613861
        }
    },
    {
        "id": 3046343,
        "name": "Pilisszentkereszt",
        "coord": {
            "lon": 18.905029,
            "lat": 47.691429
        }
    },
    {
        "id": 3046344,
        "name": "Pilisszentiván",
        "coord": {
            "lon": 18.899401,
            "lat": 47.609638
        }
    },
    {
        "id": 3046345,
        "name": "Pilisszántó",
        "coord": {
            "lon": 18.887621,
            "lat": 47.66909
        }
    },
    {
        "id": 3046346,
        "name": "Pilismarót",
        "coord": {
            "lon": 18.875389,
            "lat": 47.784641
        }
    },
    {
        "id": 3046349,
        "name": "Piliscsév",
        "coord": {
            "lon": 18.81872,
            "lat": 47.679241
        }
    },
    {
        "id": 3046353,
        "name": "Piliscsaba",
        "coord": {
            "lon": 18.828859,
            "lat": 47.634171
        }
    },
    {
        "id": 3046354,
        "name": "Pilisborosjenő",
        "coord": {
            "lon": 18.993219,
            "lat": 47.607441
        }
    },
    {
        "id": 3046355,
        "name": "Pilis",
        "coord": {
            "lon": 19.548479,
            "lat": 47.28904
        }
    },
    {
        "id": 3046387,
        "name": "Petőfibánya",
        "coord": {
            "lon": 19.7055,
            "lat": 47.779419
        }
    },
    {
        "id": 3046396,
        "name": "Pétfürdő",
        "coord": {
            "lon": 18.116671,
            "lat": 47.166672
        }
    },
    {
        "id": 3046397,
        "name": "Petesmalom",
        "coord": {
            "lon": 17.50338,
            "lat": 46.22855
        }
    },
    {
        "id": 3046409,
        "name": "Péteri",
        "coord": {
            "lon": 19.409809,
            "lat": 47.391041
        }
    },
    {
        "id": 3046431,
        "name": "Pest megye",
        "coord": {
            "lon": 19.33333,
            "lat": 47.416672
        }
    },
    {
        "id": 3046434,
        "name": "Pestiúti Kültelek",
        "coord": {
            "lon": 18.966669,
            "lat": 47.316669
        }
    },
    {
        "id": 3046446,
        "name": "Pest",
        "coord": {
            "lon": 19.08333,
            "lat": 47.5
        }
    },
    {
        "id": 3046450,
        "name": "Perőcsitanya",
        "coord": {
            "lon": 19.366671,
            "lat": 47.599998
        }
    },
    {
        "id": 3046456,
        "name": "Perkáta",
        "coord": {
            "lon": 18.78734,
            "lat": 47.047009
        }
    },
    {
        "id": 3046487,
        "name": "Perbál",
        "coord": {
            "lon": 18.76099,
            "lat": 47.589569
        }
    },
    {
        "id": 3046488,
        "name": "Pér",
        "coord": {
            "lon": 17.80632,
            "lat": 47.61153
        }
    },
    {
        "id": 3046499,
        "name": "Pellérd",
        "coord": {
            "lon": 18.15403,
            "lat": 46.034382
        }
    },
    {
        "id": 3046505,
        "name": "Pécsvárad",
        "coord": {
            "lon": 18.42321,
            "lat": 46.160332
        }
    },
    {
        "id": 3046526,
        "name": "Pécs",
        "coord": {
            "lon": 18.23333,
            "lat": 46.083328
        }
    },
    {
        "id": 3046531,
        "name": "Pécel",
        "coord": {
            "lon": 19.341619,
            "lat": 47.48962
        }
    },
    {
        "id": 3046537,
        "name": "Pázmánd",
        "coord": {
            "lon": 18.653561,
            "lat": 47.287552
        }
    },
    {
        "id": 3046545,
        "name": "Patyiház",
        "coord": {
            "lon": 17.033331,
            "lat": 47.599998
        }
    },
    {
        "id": 3046546,
        "name": "Páty",
        "coord": {
            "lon": 18.82851,
            "lat": 47.51709
        }
    },
    {
        "id": 3046577,
        "name": "Pásztorúti Dűlő",
        "coord": {
            "lon": 17.25,
            "lat": 47.616669
        }
    },
    {
        "id": 3046580,
        "name": "Pásztó",
        "coord": {
            "lon": 19.69829,
            "lat": 47.920189
        }
    },
    {
        "id": 3046619,
        "name": "Parádsasvár",
        "coord": {
            "lon": 19.977091,
            "lat": 47.912601
        }
    },
    {
        "id": 3046646,
        "name": "Pappuszta",
        "coord": {
            "lon": 18.866671,
            "lat": 46.633331
        }
    },
    {
        "id": 3046663,
        "name": "Paphegy",
        "coord": {
            "lon": 17.283331,
            "lat": 46.98333
        }
    },
    {
        "id": 3046686,
        "name": "Pápa",
        "coord": {
            "lon": 17.4674,
            "lat": 47.33004
        }
    },
    {
        "id": 3046693,
        "name": "Pannonhalma",
        "coord": {
            "lon": 17.75535,
            "lat": 47.549461
        }
    },
    {
        "id": 3046700,
        "name": "Pánd",
        "coord": {
            "lon": 19.63571,
            "lat": 47.353329
        }
    },
    {
        "id": 3046711,
        "name": "Palotás",
        "coord": {
            "lon": 19.59618,
            "lat": 47.79528
        }
    },
    {
        "id": 3046719,
        "name": "Pálmonostora",
        "coord": {
            "lon": 19.951559,
            "lat": 46.624168
        }
    },
    {
        "id": 3046768,
        "name": "Paks",
        "coord": {
            "lon": 18.859619,
            "lat": 46.62648
        }
    },
    {
        "id": 3046769,
        "name": "Pákozd",
        "coord": {
            "lon": 18.53306,
            "lat": 47.213692
        }
    },
    {
        "id": 3046800,
        "name": "Pacsa",
        "coord": {
            "lon": 17.014009,
            "lat": 46.71981
        }
    },
    {
        "id": 3046806,
        "name": "Ozora",
        "coord": {
            "lon": 18.400101,
            "lat": 46.751331
        }
    },
    {
        "id": 3046825,
        "name": "Öttevény",
        "coord": {
            "lon": 17.484739,
            "lat": 47.71946
        }
    },
    {
        "id": 3046856,
        "name": "Ősi",
        "coord": {
            "lon": 18.18833,
            "lat": 47.147221
        }
    },
    {
        "id": 3046888,
        "name": "Oroszlány",
        "coord": {
            "lon": 18.31225,
            "lat": 47.48671
        }
    },
    {
        "id": 3046916,
        "name": "Örkény",
        "coord": {
            "lon": 19.433241,
            "lat": 47.12991
        }
    },
    {
        "id": 3046922,
        "name": "Őriszentpéter",
        "coord": {
            "lon": 16.42944,
            "lat": 46.840832
        }
    },
    {
        "id": 3046940,
        "name": "Orgovány",
        "coord": {
            "lon": 19.472589,
            "lat": 46.75087
        }
    },
    {
        "id": 3047005,
        "name": "Öreghegy",
        "coord": {
            "lon": 18.133329,
            "lat": 47.116669
        }
    },
    {
        "id": 3047127,
        "name": "Őrbottyán",
        "coord": {
            "lon": 19.282391,
            "lat": 47.687111
        }
    },
    {
        "id": 3047141,
        "name": "Oncsatelep",
        "coord": {
            "lon": 18.283331,
            "lat": 46.633331
        }
    },
    {
        "id": 3047144,
        "name": "Oncsatelep",
        "coord": {
            "lon": 17.533331,
            "lat": 46.23333
        }
    },
    {
        "id": 3047201,
        "name": "Őcsény",
        "coord": {
            "lon": 18.75749,
            "lat": 46.313702
        }
    },
    {
        "id": 3047206,
        "name": "Ócsa",
        "coord": {
            "lon": 19.23057,
            "lat": 47.299858
        }
    },
    {
        "id": 3047229,
        "name": "Nyúl",
        "coord": {
            "lon": 17.689039,
            "lat": 47.590469
        }
    },
    {
        "id": 3047282,
        "name": "Nyergesújfalu",
        "coord": {
            "lon": 18.55694,
            "lat": 47.760269
        }
    },
    {
        "id": 3047309,
        "name": "Nyárlőrinc",
        "coord": {
            "lon": 19.878361,
            "lat": 46.860168
        }
    },
    {
        "id": 3047316,
        "name": "Nyáregyháza",
        "coord": {
            "lon": 19.501459,
            "lat": 47.261749
        }
    },
    {
        "id": 3047348,
        "name": "Nógrád megye",
        "coord": {
            "lon": 19.58333,
            "lat": 48.0
        }
    },
    {
        "id": 3047364,
        "name": "Neszmély",
        "coord": {
            "lon": 18.359659,
            "lat": 47.735851
        }
    },
    {
        "id": 3047373,
        "name": "Németkér",
        "coord": {
            "lon": 18.763109,
            "lat": 46.71637
        }
    },
    {
        "id": 3047386,
        "name": "Nemesvámos",
        "coord": {
            "lon": 17.874769,
            "lat": 47.055141
        }
    },
    {
        "id": 3047399,
        "name": "Nemesnádudvar",
        "coord": {
            "lon": 19.051149,
            "lat": 46.3405
        }
    },
    {
        "id": 3047428,
        "name": "Némediszőlő",
        "coord": {
            "lon": 19.22139,
            "lat": 47.337379
        }
    },
    {
        "id": 3047443,
        "name": "Naszály",
        "coord": {
            "lon": 18.257509,
            "lat": 47.703289
        }
    },
    {
        "id": 3047460,
        "name": "Nándorliget",
        "coord": {
            "lon": 18.236389,
            "lat": 45.98056
        }
    },
    {
        "id": 3047474,
        "name": "dunaújváros",
        "coord": {
            "lon": 18.93288,
            "lat": 46.967369
        }
    },
    {
        "id": 3047509,
        "name": "Nagytarcsa",
        "coord": {
            "lon": 19.28343,
            "lat": 47.531281
        }
    },
    {
        "id": 3047574,
        "name": "Nagyréde",
        "coord": {
            "lon": 19.84819,
            "lat": 47.76543
        }
    },
    {
        "id": 3047606,
        "name": "Nagyoroszi",
        "coord": {
            "lon": 19.0905,
            "lat": 48.005032
        }
    },
    {
        "id": 3047619,
        "name": "Nagymaros",
        "coord": {
            "lon": 18.959841,
            "lat": 47.792801
        }
    },
    {
        "id": 3047622,
        "name": "Nagymányok",
        "coord": {
            "lon": 18.454889,
            "lat": 46.27911
        }
    },
    {
        "id": 3047646,
        "name": "Nagykovácsi",
        "coord": {
            "lon": 19.01667,
            "lat": 47.650002
        }
    },
    {
        "id": 3047651,
        "name": "Nagykőrös",
        "coord": {
            "lon": 19.77857,
            "lat": 47.034191
        }
    },
    {
        "id": 3047669,
        "name": "Nagykáta",
        "coord": {
            "lon": 19.744101,
            "lat": 47.415138
        }
    },
    {
        "id": 3047679,
        "name": "Nagykanizsa",
        "coord": {
            "lon": 16.991039,
            "lat": 46.453468
        }
    },
    {
        "id": 3047689,
        "name": "Nagyigmánd",
        "coord": {
            "lon": 18.081301,
            "lat": 47.640942
        }
    },
    {
        "id": 3047765,
        "name": "Nagydorog",
        "coord": {
            "lon": 18.655649,
            "lat": 46.627491
        }
    },
    {
        "id": 3047781,
        "name": "Nagycenk",
        "coord": {
            "lon": 16.697321,
            "lat": 47.604351
        }
    },
    {
        "id": 3047804,
        "name": "Nagybaracska",
        "coord": {
            "lon": 18.905899,
            "lat": 46.04248
        }
    },
    {
        "id": 3047811,
        "name": "Nagybajom",
        "coord": {
            "lon": 17.511471,
            "lat": 46.392319
        }
    },
    {
        "id": 3047819,
        "name": "Nagyatád",
        "coord": {
            "lon": 17.35788,
            "lat": 46.22961
        }
    },
    {
        "id": 3047835,
        "name": "Nádormajor",
        "coord": {
            "lon": 19.133329,
            "lat": 47.633331
        }
    },
    {
        "id": 3047871,
        "name": "Murakeresztúr",
        "coord": {
            "lon": 16.881769,
            "lat": 46.36422
        }
    },
    {
        "id": 3047872,
        "name": "Murahid",
        "coord": {
            "lon": 16.73333,
            "lat": 46.433331
        }
    },
    {
        "id": 3047894,
        "name": "Mosonszentjánosi Tanyák",
        "coord": {
            "lon": 17.133329,
            "lat": 47.783329
        }
    },
    {
        "id": 3047896,
        "name": "Mosonmagyaróvár",
        "coord": {
            "lon": 17.269939,
            "lat": 47.867882
        }
    },
    {
        "id": 3047922,
        "name": "Mórahalom",
        "coord": {
            "lon": 19.885099,
            "lat": 46.21806
        }
    },
    {
        "id": 3047927,
        "name": "Mór",
        "coord": {
            "lon": 18.203529,
            "lat": 47.377869
        }
    },
    {
        "id": 3047942,
        "name": "Monor",
        "coord": {
            "lon": 19.44734,
            "lat": 47.35133
        }
    },
    {
        "id": 3047967,
        "name": "Mohács",
        "coord": {
            "lon": 18.68306,
            "lat": 45.993061
        }
    },
    {
        "id": 3047986,
        "name": "Mogyoród",
        "coord": {
            "lon": 19.2407,
            "lat": 47.597481
        }
    },
    {
        "id": 3048005,
        "name": "Mocsa",
        "coord": {
            "lon": 18.185049,
            "lat": 47.669991
        }
    },
    {
        "id": 3048068,
        "name": "Miketelep",
        "coord": {
            "lon": 17.533331,
            "lat": 46.23333
        }
    },
    {
        "id": 3048080,
        "name": "Mihályi",
        "coord": {
            "lon": 17.09507,
            "lat": 47.51384
        }
    },
    {
        "id": 3048094,
        "name": "Mezőszilas",
        "coord": {
            "lon": 18.47789,
            "lat": 46.811089
        }
    },
    {
        "id": 3048096,
        "name": "Mezőssytanya",
        "coord": {
            "lon": 19.23333,
            "lat": 47.383331
        }
    },
    {
        "id": 3048107,
        "name": "Mezőfalva",
        "coord": {
            "lon": 18.77177,
            "lat": 46.931839
        }
    },
    {
        "id": 3048186,
        "name": "Mende",
        "coord": {
            "lon": 19.45628,
            "lat": 47.43132
        }
    },
    {
        "id": 3048194,
        "name": "Mélykút",
        "coord": {
            "lon": 19.38102,
            "lat": 46.215092
        }
    },
    {
        "id": 3048250,
        "name": "Mecseknádasd",
        "coord": {
            "lon": 18.47076,
            "lat": 46.224682
        }
    },
    {
        "id": 3048281,
        "name": "Mátraverebély",
        "coord": {
            "lon": 19.780491,
            "lat": 47.974209
        }
    },
    {
        "id": 3048282,
        "name": "Mátraterenye",
        "coord": {
            "lon": 19.94762,
            "lat": 48.032669
        }
    },
    {
        "id": 3048291,
        "name": "Mátranovák",
        "coord": {
            "lon": 19.982571,
            "lat": 48.03809
        }
    },
    {
        "id": 3048326,
        "name": "Martonvásár",
        "coord": {
            "lon": 18.790449,
            "lat": 47.31601
        }
    },
    {
        "id": 3048438,
        "name": "Margitmajor",
        "coord": {
            "lon": 19.133329,
            "lat": 47.683331
        }
    },
    {
        "id": 3048444,
        "name": "Marczpuszta",
        "coord": {
            "lon": 18.533331,
            "lat": 46.299999
        }
    },
    {
        "id": 3048447,
        "name": "Marceltanya",
        "coord": {
            "lon": 19.316669,
            "lat": 47.183331
        }
    },
    {
        "id": 3048456,
        "name": "Marcali",
        "coord": {
            "lon": 17.411961,
            "lat": 46.58498
        }
    },
    {
        "id": 3048464,
        "name": "Mány",
        "coord": {
            "lon": 18.656269,
            "lat": 47.53352
        }
    },
    {
        "id": 3048591,
        "name": "Magyarlak",
        "coord": {
            "lon": 16.34766,
            "lat": 46.951672
        }
    },
    {
        "id": 3048627,
        "name": "Mágocs",
        "coord": {
            "lon": 18.232401,
            "lat": 46.349979
        }
    },
    {
        "id": 3048630,
        "name": "Maglód",
        "coord": {
            "lon": 19.36438,
            "lat": 47.442581
        }
    },
    {
        "id": 3048647,
        "name": "Madocsa",
        "coord": {
            "lon": 18.957911,
            "lat": 46.687901
        }
    },
    {
        "id": 3048657,
        "name": "Madaras",
        "coord": {
            "lon": 19.261209,
            "lat": 46.058701
        }
    },
    {
        "id": 3048713,
        "name": "Lovasberény",
        "coord": {
            "lon": 18.551769,
            "lat": 47.309971
        }
    },
    {
        "id": 3048725,
        "name": "Lőrinci",
        "coord": {
            "lon": 19.678671,
            "lat": 47.732948
        }
    },
    {
        "id": 3048762,
        "name": "Litér",
        "coord": {
            "lon": 18.004539,
            "lat": 47.10104
        }
    },
    {
        "id": 3048820,
        "name": "Letenye",
        "coord": {
            "lon": 16.72583,
            "lat": 46.43301
        }
    },
    {
        "id": 3048835,
        "name": "Lepsény",
        "coord": {
            "lon": 18.24358,
            "lat": 46.99036
        }
    },
    {
        "id": 3048847,
        "name": "Lenti",
        "coord": {
            "lon": 16.53863,
            "lat": 46.624031
        }
    },
    {
        "id": 3048852,
        "name": "Lengyeltóti",
        "coord": {
            "lon": 17.64398,
            "lat": 46.670132
        }
    },
    {
        "id": 3048888,
        "name": "Lébény",
        "coord": {
            "lon": 17.39076,
            "lat": 47.735741
        }
    },
    {
        "id": 3048895,
        "name": "Leányfalu",
        "coord": {
            "lon": 19.08585,
            "lat": 47.717781
        }
    },
    {
        "id": 3048963,
        "name": "Lánycsók",
        "coord": {
            "lon": 18.625259,
            "lat": 46.005428
        }
    },
    {
        "id": 3048984,
        "name": "Lakitelek",
        "coord": {
            "lon": 19.995041,
            "lat": 46.876011
        }
    },
    {
        "id": 3049002,
        "name": "Lajosmizse",
        "coord": {
            "lon": 19.56171,
            "lat": 47.021332
        }
    },
    {
        "id": 3049013,
        "name": "Lajoskomárom",
        "coord": {
            "lon": 18.337629,
            "lat": 46.841999
        }
    },
    {
        "id": 3049033,
        "name": "Lábod",
        "coord": {
            "lon": 17.454189,
            "lat": 46.205349
        }
    },
    {
        "id": 3049036,
        "name": "Lábatlan",
        "coord": {
            "lon": 18.496229,
            "lat": 47.747349
        }
    },
    {
        "id": 3049041,
        "name": "Kutyahegy",
        "coord": {
            "lon": 18.816669,
            "lat": 47.466671
        }
    },
    {
        "id": 3049100,
        "name": "Kunszentmiklós",
        "coord": {
            "lon": 19.12575,
            "lat": 47.027
        }
    },
    {
        "id": 3049111,
        "name": "Kunfehértó",
        "coord": {
            "lon": 19.414539,
            "lat": 46.360909
        }
    },
    {
        "id": 3049201,
        "name": "Központimajor",
        "coord": {
            "lon": 19.366671,
            "lat": 47.599998
        }
    },
    {
        "id": 3049277,
        "name": "Kozármisleny",
        "coord": {
            "lon": 18.292101,
            "lat": 46.029671
        }
    },
    {
        "id": 3049366,
        "name": "Kőszeg",
        "coord": {
            "lon": 16.541,
            "lat": 47.389221
        }
    },
    {
        "id": 3049377,
        "name": "Kosd",
        "coord": {
            "lon": 19.178209,
            "lat": 47.807911
        }
    },
    {
        "id": 3049380,
        "name": "Kosárhegy",
        "coord": {
            "lon": 19.799999,
            "lat": 47.166672
        }
    },
    {
        "id": 3049421,
        "name": "Környe",
        "coord": {
            "lon": 18.320801,
            "lat": 47.54668
        }
    },
    {
        "id": 3049430,
        "name": "Körmend",
        "coord": {
            "lon": 16.605961,
            "lat": 47.01096
        }
    },
    {
        "id": 3049491,
        "name": "Kóny",
        "coord": {
            "lon": 17.35717,
            "lat": 47.630531
        }
    },
    {
        "id": 3049512,
        "name": "Komló",
        "coord": {
            "lon": 18.26494,
            "lat": 46.19278
        }
    },
    {
        "id": 3049518,
        "name": "Komárom-Esztergom",
        "coord": {
            "lon": 18.33333,
            "lat": 47.583328
        }
    },
    {
        "id": 3049519,
        "name": "Komárom",
        "coord": {
            "lon": 18.119129,
            "lat": 47.743179
        }
    },
    {
        "id": 3049570,
        "name": "Kokashegy",
        "coord": {
            "lon": 17.66667,
            "lat": 46.76667
        }
    },
    {
        "id": 3049574,
        "name": "Kóka",
        "coord": {
            "lon": 19.57876,
            "lat": 47.485519
        }
    },
    {
        "id": 3049630,
        "name": "Kocsér",
        "coord": {
            "lon": 19.92067,
            "lat": 47.001652
        }
    },
    {
        "id": 3049631,
        "name": "Kocs",
        "coord": {
            "lon": 18.21517,
            "lat": 47.607498
        }
    },
    {
        "id": 3049647,
        "name": "Kőbánya",
        "coord": {
            "lon": 18.816669,
            "lat": 47.466671
        }
    },
    {
        "id": 3049719,
        "name": "Kistelek",
        "coord": {
            "lon": 19.979719,
            "lat": 46.4725
        }
    },
    {
        "id": 3049751,
        "name": "Kisszállás",
        "coord": {
            "lon": 19.48954,
            "lat": 46.28009
        }
    },
    {
        "id": 3049860,
        "name": "Kisláng",
        "coord": {
            "lon": 18.38813,
            "lat": 46.957439
        }
    },
    {
        "id": 3049875,
        "name": "Kiskunmajsa",
        "coord": {
            "lon": 19.74,
            "lat": 46.49028
        }
    },
    {
        "id": 3049878,
        "name": "Kiskunlacháza",
        "coord": {
            "lon": 19.0093,
            "lat": 47.188389
        }
    },
    {
        "id": 3049880,
        "name": "Kiskunhalas",
        "coord": {
            "lon": 19.4848,
            "lat": 46.434021
        }
    },
    {
        "id": 3049882,
        "name": "Kiskunfélegyházi Tanyák",
        "coord": {
            "lon": 19.85,
            "lat": 46.716671
        }
    },
    {
        "id": 3049885,
        "name": "Kiskunfélegyháza",
        "coord": {
            "lon": 19.84458,
            "lat": 46.712132
        }
    },
    {
        "id": 3049896,
        "name": "Kiskőrös",
        "coord": {
            "lon": 19.28528,
            "lat": 46.621391
        }
    },
    {
        "id": 3050022,
        "name": "Kisfái",
        "coord": {
            "lon": 19.79775,
            "lat": 46.89156
        }
    },
    {
        "id": 3050052,
        "name": "Kiscsőszi Tanya",
        "coord": {
            "lon": 17.283331,
            "lat": 47.200001
        }
    },
    {
        "id": 3050053,
        "name": "Kiscsősz",
        "coord": {
            "lon": 17.28006,
            "lat": 47.195641
        }
    },
    {
        "id": 3050088,
        "name": "Kisbér",
        "coord": {
            "lon": 18.03665,
            "lat": 47.502892
        }
    },
    {
        "id": 3050160,
        "name": "Kincsesbánya",
        "coord": {
            "lon": 18.277901,
            "lat": 47.264439
        }
    },
    {
        "id": 3050165,
        "name": "Kimle",
        "coord": {
            "lon": 17.366421,
            "lat": 47.817261
        }
    },
    {
        "id": 3050204,
        "name": "Kéthely",
        "coord": {
            "lon": 17.39362,
            "lat": 46.646049
        }
    },
    {
        "id": 3050208,
        "name": "Kesztölc",
        "coord": {
            "lon": 18.801769,
            "lat": 47.712662
        }
    },
    {
        "id": 3050212,
        "name": "Keszthely",
        "coord": {
            "lon": 17.24317,
            "lat": 46.76812
        }
    },
    {
        "id": 3050290,
        "name": "Kistarcsa",
        "coord": {
            "lon": 19.26247,
            "lat": 47.547569
        }
    },
    {
        "id": 3050311,
        "name": "Kerekegyháza",
        "coord": {
            "lon": 19.47806,
            "lat": 46.937222
        }
    },
    {
        "id": 3050406,
        "name": "Kelebia",
        "coord": {
            "lon": 19.61659,
            "lat": 46.1968
        }
    },
    {
        "id": 3050434,
        "name": "Kecskemét",
        "coord": {
            "lon": 19.69128,
            "lat": 46.906181
        }
    },
    {
        "id": 3050447,
        "name": "Kecskéd",
        "coord": {
            "lon": 18.309389,
            "lat": 47.52298
        }
    },
    {
        "id": 3050454,
        "name": "Kecel",
        "coord": {
            "lon": 19.25194,
            "lat": 46.52528
        }
    },
    {
        "id": 3050459,
        "name": "Kazár",
        "coord": {
            "lon": 19.861429,
            "lat": 48.049519
        }
    },
    {
        "id": 3050466,
        "name": "Kavicsbánya",
        "coord": {
            "lon": 19.283331,
            "lat": 47.400002
        }
    },
    {
        "id": 3050475,
        "name": "Katymár",
        "coord": {
            "lon": 19.209351,
            "lat": 46.033981
        }
    },
    {
        "id": 3050526,
        "name": "Kartal",
        "coord": {
            "lon": 19.542,
            "lat": 47.671329
        }
    },
    {
        "id": 3050550,
        "name": "Károlyakna",
        "coord": {
            "lon": 19.799999,
            "lat": 48.116669
        }
    },
    {
        "id": 3050572,
        "name": "Karancslapujtő",
        "coord": {
            "lon": 19.73333,
            "lat": 48.150002
        }
    },
    {
        "id": 3050573,
        "name": "Karancskeszi",
        "coord": {
            "lon": 19.696859,
            "lat": 48.163528
        }
    },
    {
        "id": 3050588,
        "name": "Karád",
        "coord": {
            "lon": 17.84136,
            "lat": 46.690762
        }
    },
    {
        "id": 3050594,
        "name": "Kapuvár",
        "coord": {
            "lon": 17.02886,
            "lat": 47.592239
        }
    },
    {
        "id": 3050616,
        "name": "Kaposvár",
        "coord": {
            "lon": 17.799999,
            "lat": 46.366669
        }
    },
    {
        "id": 3050627,
        "name": "Kaposmérő",
        "coord": {
            "lon": 17.704,
            "lat": 46.361671
        }
    },
    {
        "id": 3050647,
        "name": "Kápolnásnyék",
        "coord": {
            "lon": 18.67564,
            "lat": 47.24004
        }
    },
    {
        "id": 3050710,
        "name": "Kálvária Dűlő",
        "coord": {
            "lon": 18.73333,
            "lat": 47.716671
        }
    },
    {
        "id": 3050714,
        "name": "Káloz",
        "coord": {
            "lon": 18.48259,
            "lat": 46.954639
        }
    },
    {
        "id": 3050719,
        "name": "Kalocsa",
        "coord": {
            "lon": 18.98583,
            "lat": 46.52639
        }
    },
    {
        "id": 3050750,
        "name": "Kakucs",
        "coord": {
            "lon": 19.36467,
            "lat": 47.242001
        }
    },
    {
        "id": 3050785,
        "name": "Kadarkút",
        "coord": {
            "lon": 17.62014,
            "lat": 46.236229
        }
    },
    {
        "id": 3050828,
        "name": "Józsefpuszta",
        "coord": {
            "lon": 18.200001,
            "lat": 47.383331
        }
    },
    {
        "id": 3050872,
        "name": "Jobbágyi",
        "coord": {
            "lon": 19.67762,
            "lat": 47.832378
        }
    },
    {
        "id": 3050901,
        "name": "Jászszentlászló",
        "coord": {
            "lon": 19.760651,
            "lat": 46.566849
        }
    },
    {
        "id": 3050907,
        "name": "Jászberény",
        "coord": {
            "lon": 19.91667,
            "lat": 47.5
        }
    },
    {
        "id": 3050909,
        "name": "Jászárokszállás",
        "coord": {
            "lon": 19.980379,
            "lat": 47.64238
        }
    },
    {
        "id": 3050922,
        "name": "Jansentanya",
        "coord": {
            "lon": 18.91667,
            "lat": 47.616669
        }
    },
    {
        "id": 3050927,
        "name": "Jánossomorja",
        "coord": {
            "lon": 17.13603,
            "lat": 47.786209
        }
    },
    {
        "id": 3050964,
        "name": "Jánosháza",
        "coord": {
            "lon": 17.16503,
            "lat": 47.11937
        }
    },
    {
        "id": 3050967,
        "name": "Jánoshalma",
        "coord": {
            "lon": 19.32583,
            "lat": 46.298611
        }
    },
    {
        "id": 3050997,
        "name": "Fülöpjakab",
        "coord": {
            "lon": 19.721319,
            "lat": 46.74221
        }
    },
    {
        "id": 3051001,
        "name": "Ják",
        "coord": {
            "lon": 16.58148,
            "lat": 47.142502
        }
    },
    {
        "id": 3051016,
        "name": "Izsák",
        "coord": {
            "lon": 19.351721,
            "lat": 46.804539
        }
    },
    {
        "id": 3051018,
        "name": "Izguntanya",
        "coord": {
            "lon": 18.783331,
            "lat": 46.433331
        }
    },
    {
        "id": 3051035,
        "name": "Iváncsa",
        "coord": {
            "lon": 18.820299,
            "lat": 47.1567
        }
    },
    {
        "id": 3051077,
        "name": "Isaszeg",
        "coord": {
            "lon": 19.40205,
            "lat": 47.530109
        }
    },
    {
        "id": 3051100,
        "name": "Iregszemcse",
        "coord": {
            "lon": 18.18581,
            "lat": 46.69286
        }
    },
    {
        "id": 3051133,
        "name": "Inárcs",
        "coord": {
            "lon": 19.327,
            "lat": 47.262001
        }
    },
    {
        "id": 3051187,
        "name": "Iklad",
        "coord": {
            "lon": 19.4361,
            "lat": 47.66534
        }
    },
    {
        "id": 3051280,
        "name": "Hosszúhetény",
        "coord": {
            "lon": 18.350771,
            "lat": 46.164139
        }
    },
    {
        "id": 3051294,
        "name": "Hosszúhát",
        "coord": {
            "lon": 19.133329,
            "lat": 47.033329
        }
    },
    {
        "id": 3051324,
        "name": "Hort",
        "coord": {
            "lon": 19.789301,
            "lat": 47.690811
        }
    },
    {
        "id": 3051415,
        "name": "Hőgyész",
        "coord": {
            "lon": 18.418409,
            "lat": 46.496971
        }
    },
    {
        "id": 3051473,
        "name": "Hidas",
        "coord": {
            "lon": 18.495399,
            "lat": 46.256802
        }
    },
    {
        "id": 3051476,
        "name": "Hévízgyörk",
        "coord": {
            "lon": 19.51667,
            "lat": 47.633331
        }
    },
    {
        "id": 3051477,
        "name": "Hévíz",
        "coord": {
            "lon": 17.18408,
            "lat": 46.79031
        }
    },
    {
        "id": 3051499,
        "name": "Hernád",
        "coord": {
            "lon": 19.432949,
            "lat": 47.16238
        }
    },
    {
        "id": 3051512,
        "name": "Herend",
        "coord": {
            "lon": 17.75,
            "lat": 47.133331
        }
    },
    {
        "id": 3051516,
        "name": "Heréd",
        "coord": {
            "lon": 19.633141,
            "lat": 47.706379
        }
    },
    {
        "id": 3051520,
        "name": "Hercegszántó",
        "coord": {
            "lon": 18.939171,
            "lat": 45.950001
        }
    },
    {
        "id": 3051535,
        "name": "Helvécia",
        "coord": {
            "lon": 19.622511,
            "lat": 46.836609
        }
    },
    {
        "id": 3051597,
        "name": "Hegy",
        "coord": {
            "lon": 19.616671,
            "lat": 47.25
        }
    },
    {
        "id": 3051621,
        "name": "Hatvan",
        "coord": {
            "lon": 19.683331,
            "lat": 47.666672
        }
    },
    {
        "id": 3051657,
        "name": "Harta",
        "coord": {
            "lon": 19.030531,
            "lat": 46.69846
        }
    },
    {
        "id": 3051718,
        "name": "Harkány",
        "coord": {
            "lon": 18.236679,
            "lat": 45.850021
        }
    },
    {
        "id": 3051774,
        "name": "Hangyásmál",
        "coord": {
            "lon": 17.799999,
            "lat": 46.366669
        }
    },
    {
        "id": 3051788,
        "name": "Hamvasbánya",
        "coord": {
            "lon": 18.5,
            "lat": 47.75
        }
    },
    {
        "id": 3051822,
        "name": "Halásztelek",
        "coord": {
            "lon": 18.98119,
            "lat": 47.361729
        }
    },
    {
        "id": 3051829,
        "name": "Halászi",
        "coord": {
            "lon": 17.326151,
            "lat": 47.889301
        }
    },
    {
        "id": 3051839,
        "name": "Halastó",
        "coord": {
            "lon": 19.299999,
            "lat": 48.083328
        }
    },
    {
        "id": 3051873,
        "name": "Hajós",
        "coord": {
            "lon": 19.12056,
            "lat": 46.398609
        }
    },
    {
        "id": 3051881,
        "name": "Hajmáskér",
        "coord": {
            "lon": 18.01964,
            "lat": 47.14513
        }
    },
    {
        "id": 3051965,
        "name": "Győrújbarát",
        "coord": {
            "lon": 17.64875,
            "lat": 47.606419
        }
    },
    {
        "id": 3051969,
        "name": "Győrszemere",
        "coord": {
            "lon": 17.56356,
            "lat": 47.552559
        }
    },
    {
        "id": 3051977,
        "name": "Győr-Moson-Sopron megye",
        "coord": {
            "lon": 17.25,
            "lat": 47.666672
        }
    },
    {
        "id": 3052009,
        "name": "Győr",
        "coord": {
            "lon": 17.63512,
            "lat": 47.683331
        }
    },
    {
        "id": 3052013,
        "name": "Gyönk",
        "coord": {
            "lon": 18.47694,
            "lat": 46.55603
        }
    },
    {
        "id": 3052021,
        "name": "Gyöngyöstarján",
        "coord": {
            "lon": 19.867241,
            "lat": 47.81369
        }
    },
    {
        "id": 3052023,
        "name": "Gyöngyössolymos",
        "coord": {
            "lon": 19.93619,
            "lat": 47.817242
        }
    },
    {
        "id": 3052026,
        "name": "Gyöngyöspata",
        "coord": {
            "lon": 19.78924,
            "lat": 47.815048
        }
    },
    {
        "id": 3052035,
        "name": "Gyöngyöshalász",
        "coord": {
            "lon": 19.928761,
            "lat": 47.741611
        }
    },
    {
        "id": 3052040,
        "name": "Gyöngyös",
        "coord": {
            "lon": 19.927999,
            "lat": 47.78257
        }
    },
    {
        "id": 3052045,
        "name": "Gyömrő",
        "coord": {
            "lon": 19.401331,
            "lat": 47.42733
        }
    },
    {
        "id": 3052069,
        "name": "Gyenesdiás",
        "coord": {
            "lon": 17.2866,
            "lat": 46.77058
        }
    },
    {
        "id": 3052101,
        "name": "Gyál",
        "coord": {
            "lon": 19.221399,
            "lat": 47.384491
        }
    },
    {
        "id": 3052208,
        "name": "Gomba",
        "coord": {
            "lon": 19.530569,
            "lat": 47.370949
        }
    },
    {
        "id": 3052236,
        "name": "Gödöllő",
        "coord": {
            "lon": 19.35515,
            "lat": 47.596569
        }
    },
    {
        "id": 3052241,
        "name": "Göd",
        "coord": {
            "lon": 19.134171,
            "lat": 47.683239
        }
    },
    {
        "id": 3052267,
        "name": "Gindeletanya",
        "coord": {
            "lon": 19.700001,
            "lat": 47.916672
        }
    },
    {
        "id": 3052330,
        "name": "Gencsapáti",
        "coord": {
            "lon": 16.595751,
            "lat": 47.284962
        }
    },
    {
        "id": 3052366,
        "name": "Gátér",
        "coord": {
            "lon": 19.783331,
            "lat": 47.033329
        }
    },
    {
        "id": 3052381,
        "name": "Gárdony",
        "coord": {
            "lon": 18.63607,
            "lat": 47.209419
        }
    },
    {
        "id": 3052391,
        "name": "Gara",
        "coord": {
            "lon": 19.04278,
            "lat": 46.03194
        }
    },
    {
        "id": 3052417,
        "name": "Galgamácsa",
        "coord": {
            "lon": 19.387239,
            "lat": 47.695621
        }
    },
    {
        "id": 3052419,
        "name": "Galgahévíz",
        "coord": {
            "lon": 19.566669,
            "lat": 47.616669
        }
    },
    {
        "id": 3052500,
        "name": "Fülöpszállás",
        "coord": {
            "lon": 19.23748,
            "lat": 46.820751
        }
    },
    {
        "id": 3052526,
        "name": "Franktanya",
        "coord": {
            "lon": 18.961969,
            "lat": 47.446201
        }
    },
    {
        "id": 3052542,
        "name": "Fót",
        "coord": {
            "lon": 19.1887,
            "lat": 47.617699
        }
    },
    {
        "id": 3052545,
        "name": "Forrópuszta",
        "coord": {
            "lon": 19.66,
            "lat": 47.45504
        }
    },
    {
        "id": 3052553,
        "name": "Forráskút",
        "coord": {
            "lon": 19.909731,
            "lat": 46.36528
        }
    },
    {
        "id": 3052574,
        "name": "Fonyód",
        "coord": {
            "lon": 17.579451,
            "lat": 46.75552
        }
    },
    {
        "id": 3052614,
        "name": "Flinkmalom",
        "coord": {
            "lon": 18.133329,
            "lat": 47.116669
        }
    },
    {
        "id": 3052639,
        "name": "Fertőszentmiklós",
        "coord": {
            "lon": 16.87517,
            "lat": 47.589958
        }
    },
    {
        "id": 3052640,
        "name": "Fertőrákos",
        "coord": {
            "lon": 16.6504,
            "lat": 47.720169
        }
    },
    {
        "id": 3052645,
        "name": "Fertőd",
        "coord": {
            "lon": 16.886761,
            "lat": 47.619469
        }
    },
    {
        "id": 3052717,
        "name": "Felsőtelep",
        "coord": {
            "lon": 19.47806,
            "lat": 46.561211
        }
    },
    {
        "id": 3052729,
        "name": "Felsőtag",
        "coord": {
            "lon": 19.049999,
            "lat": 47.349998
        }
    },
    {
        "id": 3052749,
        "name": "Felsőszentiván",
        "coord": {
            "lon": 19.186859,
            "lat": 46.197128
        }
    },
    {
        "id": 3052786,
        "name": "Felsőpakony",
        "coord": {
            "lon": 19.23698,
            "lat": 47.343281
        }
    },
    {
        "id": 3052854,
        "name": "Felsőkolónia",
        "coord": {
            "lon": 17.51667,
            "lat": 46.799999
        }
    },
    {
        "id": 3052894,
        "name": "Felsőhalom",
        "coord": {
            "lon": 19.23333,
            "lat": 47.416672
        }
    },
    {
        "id": 3052963,
        "name": "Felsőbányatelep",
        "coord": {
            "lon": 18.316669,
            "lat": 47.48333
        }
    },
    {
        "id": 3053028,
        "name": "Fejér megye",
        "coord": {
            "lon": 18.58333,
            "lat": 47.166672
        }
    },
    {
        "id": 3053033,
        "name": "Fehérvárcsurgó",
        "coord": {
            "lon": 18.264601,
            "lat": 47.293491
        }
    },
    {
        "id": 3053073,
        "name": "Farmos",
        "coord": {
            "lon": 19.846189,
            "lat": 47.360668
        }
    },
    {
        "id": 3053085,
        "name": "Farkasmály",
        "coord": {
            "lon": 19.933331,
            "lat": 47.783329
        }
    },
    {
        "id": 3053109,
        "name": "Farád",
        "coord": {
            "lon": 17.200239,
            "lat": 47.606331
        }
    },
    {
        "id": 3053132,
        "name": "Fadd",
        "coord": {
            "lon": 18.81925,
            "lat": 46.46476
        }
    },
    {
        "id": 3053147,
        "name": "Etyek",
        "coord": {
            "lon": 18.753281,
            "lat": 47.447941
        }
    },
    {
        "id": 3053163,
        "name": "Esztergom",
        "coord": {
            "lon": 18.74148,
            "lat": 47.792801
        }
    },
    {
        "id": 3053199,
        "name": "Érsekvadkert",
        "coord": {
            "lon": 19.202311,
            "lat": 47.996189
        }
    },
    {
        "id": 3053203,
        "name": "Érsekcsanád",
        "coord": {
            "lon": 18.98457,
            "lat": 46.253521
        }
    },
    {
        "id": 3053228,
        "name": "Érd-Parkváros",
        "coord": {
            "lon": 18.90391,
            "lat": 47.410431
        }
    },
    {
        "id": 3053253,
        "name": "Erdőkertes",
        "coord": {
            "lon": 19.307859,
            "lat": 47.672611
        }
    },
    {
        "id": 3053281,
        "name": "Érd",
        "coord": {
            "lon": 18.913601,
            "lat": 47.39489
        }
    },
    {
        "id": 3053283,
        "name": "Ercsi",
        "coord": {
            "lon": 18.896231,
            "lat": 47.251942
        }
    },
    {
        "id": 3053297,
        "name": "Enying",
        "coord": {
            "lon": 18.24202,
            "lat": 46.930462
        }
    },
    {
        "id": 3053321,
        "name": "Előszállás",
        "coord": {
            "lon": 18.83481,
            "lat": 46.83094
        }
    },
    {
        "id": 3053402,
        "name": "Ecser",
        "coord": {
            "lon": 19.324499,
            "lat": 47.44389
        }
    },
    {
        "id": 3053406,
        "name": "Ecséd",
        "coord": {
            "lon": 19.76696,
            "lat": 47.73267
        }
    },
    {
        "id": 3053423,
        "name": "Dusnok",
        "coord": {
            "lon": 18.962959,
            "lat": 46.39085
        }
    },
    {
        "id": 3053432,
        "name": "Dunavecse",
        "coord": {
            "lon": 18.974331,
            "lat": 46.91407
        }
    },
    {
        "id": 3053434,
        "name": "Dunavarsány",
        "coord": {
            "lon": 19.06617,
            "lat": 47.278591
        }
    },
    {
        "id": 3053438,
        "name": "Dunaújváros",
        "coord": {
            "lon": 18.933331,
            "lat": 46.98333
        }
    },
    {
        "id": 3053450,
        "name": "Dunaszentgyörgy",
        "coord": {
            "lon": 18.817711,
            "lat": 46.528519
        }
    },
    {
        "id": 3053454,
        "name": "Dunaszekcső",
        "coord": {
            "lon": 18.75865,
            "lat": 46.086632
        }
    },
    {
        "id": 3053459,
        "name": "Dunapataj",
        "coord": {
            "lon": 18.996321,
            "lat": 46.644001
        }
    },
    {
        "id": 3053476,
        "name": "Dunakeszi",
        "coord": {
            "lon": 19.138639,
            "lat": 47.636398
        }
    },
    {
        "id": 3053483,
        "name": "Dunaharaszti Rév",
        "coord": {
            "lon": 19.049999,
            "lat": 47.349998
        }
    },
    {
        "id": 3053485,
        "name": "Dunaharaszti",
        "coord": {
            "lon": 19.098221,
            "lat": 47.3545
        }
    },
    {
        "id": 3053489,
        "name": "Dunaföldvár",
        "coord": {
            "lon": 18.92638,
            "lat": 46.807999
        }
    },
    {
        "id": 3053495,
        "name": "Dunabogdány",
        "coord": {
            "lon": 19.04125,
            "lat": 47.79052
        }
    },
    {
        "id": 3053497,
        "name": "Dunaalmás",
        "coord": {
            "lon": 18.32235,
            "lat": 47.72868
        }
    },
    {
        "id": 3053502,
        "name": "Dugdelpuszta",
        "coord": {
            "lon": 19.776581,
            "lat": 48.116001
        }
    },
    {
        "id": 3053562,
        "name": "Dorog",
        "coord": {
            "lon": 18.736401,
            "lat": 47.720951
        }
    },
    {
        "id": 3053577,
        "name": "Dömsöd",
        "coord": {
            "lon": 19.011061,
            "lat": 47.09005
        }
    },
    {
        "id": 3053583,
        "name": "Domony",
        "coord": {
            "lon": 19.432289,
            "lat": 47.655521
        }
    },
    {
        "id": 3053590,
        "name": "Dombóvár",
        "coord": {
            "lon": 18.136959,
            "lat": 46.376572
        }
    },
    {
        "id": 3053595,
        "name": "Dombhátdűlő",
        "coord": {
            "lon": 19.08333,
            "lat": 47.666672
        }
    },
    {
        "id": 3053611,
        "name": "Dognyai Tanya",
        "coord": {
            "lon": 19.366671,
            "lat": 47.450001
        }
    },
    {
        "id": 3053623,
        "name": "Döbrököz",
        "coord": {
            "lon": 18.239519,
            "lat": 46.42178
        }
    },
    {
        "id": 3053682,
        "name": "Diósjenő",
        "coord": {
            "lon": 19.043171,
            "lat": 47.939678
        }
    },
    {
        "id": 3053684,
        "name": "Diósd",
        "coord": {
            "lon": 18.94898,
            "lat": 47.4095
        }
    },
    {
        "id": 3053712,
        "name": "Devecser",
        "coord": {
            "lon": 17.438021,
            "lat": 47.103161
        }
    },
    {
        "id": 3053760,
        "name": "Délegyháza",
        "coord": {
            "lon": 19.090191,
            "lat": 47.241348
        }
    },
    {
        "id": 3053764,
        "name": "Dég",
        "coord": {
            "lon": 18.45042,
            "lat": 46.868069
        }
    },
    {
        "id": 3053768,
        "name": "Decs",
        "coord": {
            "lon": 18.76,
            "lat": 46.284279
        }
    },
    {
        "id": 3053779,
        "name": "Dávod",
        "coord": {
            "lon": 18.917219,
            "lat": 45.994999
        }
    },
    {
        "id": 3053809,
        "name": "Dány",
        "coord": {
            "lon": 19.544001,
            "lat": 47.52
        }
    },
    {
        "id": 3053810,
        "name": "Dánszentmiklós",
        "coord": {
            "lon": 19.546949,
            "lat": 47.214859
        }
    },
    {
        "id": 3053836,
        "name": "Dabas",
        "coord": {
            "lon": 19.310909,
            "lat": 47.18594
        }
    },
    {
        "id": 3053863,
        "name": "Csurgó",
        "coord": {
            "lon": 17.100599,
            "lat": 46.253139
        }
    },
    {
        "id": 3053876,
        "name": "Csukásitanyák",
        "coord": {
            "lon": 19.316669,
            "lat": 47.5
        }
    },
    {
        "id": 3053918,
        "name": "Csorna",
        "coord": {
            "lon": 17.25012,
            "lat": 47.611549
        }
    },
    {
        "id": 3053938,
        "name": "Csopak",
        "coord": {
            "lon": 17.91819,
            "lat": 46.977089
        }
    },
    {
        "id": 3053971,
        "name": "Csömör",
        "coord": {
            "lon": 19.23333,
            "lat": 47.549999
        }
    },
    {
        "id": 3053986,
        "name": "Csolnok",
        "coord": {
            "lon": 18.71611,
            "lat": 47.691158
        }
    },
    {
        "id": 3054027,
        "name": "Csobánka",
        "coord": {
            "lon": 18.961889,
            "lat": 47.64637
        }
    },
    {
        "id": 3054144,
        "name": "Csetény",
        "coord": {
            "lon": 17.992081,
            "lat": 47.318062
        }
    },
    {
        "id": 3054164,
        "name": "Cserszegtomaj",
        "coord": {
            "lon": 17.220961,
            "lat": 46.801651
        }
    },
    {
        "id": 3054211,
        "name": "Cserhátpuszta",
        "coord": {
            "lon": 17.49654,
            "lat": 46.257179
        }
    },
    {
        "id": 3054240,
        "name": "Cserepes",
        "coord": {
            "lon": 18.966669,
            "lat": 46.183331
        }
    },
    {
        "id": 3054260,
        "name": "Csepreg",
        "coord": {
            "lon": 16.708811,
            "lat": 47.400982
        }
    },
    {
        "id": 3054280,
        "name": "Csengőd",
        "coord": {
            "lon": 19.268021,
            "lat": 46.715431
        }
    },
    {
        "id": 3054287,
        "name": "Csengele",
        "coord": {
            "lon": 19.863581,
            "lat": 46.542339
        }
    },
    {
        "id": 3054296,
        "name": "Csemő",
        "coord": {
            "lon": 19.69092,
            "lat": 47.117989
        }
    },
    {
        "id": 3054328,
        "name": "Csávoly",
        "coord": {
            "lon": 19.146669,
            "lat": 46.189171
        }
    },
    {
        "id": 3054352,
        "name": "Császártöltés",
        "coord": {
            "lon": 19.183611,
            "lat": 46.42194
        }
    },
    {
        "id": 3054359,
        "name": "Császár",
        "coord": {
            "lon": 18.142719,
            "lat": 47.498001
        }
    },
    {
        "id": 3054380,
        "name": "Csárda",
        "coord": {
            "lon": 17.48333,
            "lat": 47.083328
        }
    },
    {
        "id": 3054400,
        "name": "Csány",
        "coord": {
            "lon": 19.82972,
            "lat": 47.648289
        }
    },
    {
        "id": 3054434,
        "name": "Csákvár",
        "coord": {
            "lon": 18.46501,
            "lat": 47.391842
        }
    },
    {
        "id": 3054456,
        "name": "Csabrendek",
        "coord": {
            "lon": 17.29108,
            "lat": 47.013561
        }
    },
    {
        "id": 3054533,
        "name": "Celldömölk",
        "coord": {
            "lon": 17.15027,
            "lat": 47.257118
        }
    },
    {
        "id": 3054542,
        "name": "Ceglédbercel",
        "coord": {
            "lon": 19.66828,
            "lat": 47.223701
        }
    },
    {
        "id": 3054543,
        "name": "Cegléd",
        "coord": {
            "lon": 19.79952,
            "lat": 47.172661
        }
    },
    {
        "id": 3054547,
        "name": "Cece",
        "coord": {
            "lon": 18.62826,
            "lat": 46.770561
        }
    },
    {
        "id": 3054586,
        "name": "Bükkösd",
        "coord": {
            "lon": 17.988199,
            "lat": 46.10751
        }
    },
    {
        "id": 3054597,
        "name": "Bük",
        "coord": {
            "lon": 16.75065,
            "lat": 47.384861
        }
    },
    {
        "id": 3054600,
        "name": "Buják",
        "coord": {
            "lon": 19.54381,
            "lat": 47.883518
        }
    },
    {
        "id": 3054606,
        "name": "Bugyi",
        "coord": {
            "lon": 19.146641,
            "lat": 47.227482
        }
    },
    {
        "id": 3054612,
        "name": "Bugac",
        "coord": {
            "lon": 19.68074,
            "lat": 46.687038
        }
    },
    {
        "id": 3054638,
        "name": "Budapest",
        "coord": {
            "lon": 19.08333,
            "lat": 47.5
        }
    },
    {
        "id": 3054643,
        "name": "Budapest",
        "coord": {
            "lon": 19.039909,
            "lat": 47.498009
        }
    },
    {
        "id": 3054646,
        "name": "Budaörs",
        "coord": {
            "lon": 18.95845,
            "lat": 47.461811
        }
    },
    {
        "id": 3054648,
        "name": "Budakeszi",
        "coord": {
            "lon": 18.92717,
            "lat": 47.51083
        }
    },
    {
        "id": 3054651,
        "name": "Budakalász",
        "coord": {
            "lon": 19.049999,
            "lat": 47.616669
        }
    },
    {
        "id": 3054693,
        "name": "Aszód",
        "coord": {
            "lon": 19.4785,
            "lat": 47.651741
        }
    },
    {
        "id": 3054713,
        "name": "Bothmerpuszta",
        "coord": {
            "lon": 17.83333,
            "lat": 47.650002
        }
    },
    {
        "id": 3054726,
        "name": "Bősárkány",
        "coord": {
            "lon": 17.25,
            "lat": 47.688202
        }
    },
    {
        "id": 3054795,
        "name": "Bordány",
        "coord": {
            "lon": 19.923059,
            "lat": 46.318432
        }
    },
    {
        "id": 3054800,
        "name": "Borbás",
        "coord": {
            "lon": 19.804779,
            "lat": 46.936562
        }
    },
    {
        "id": 3054809,
        "name": "Bonyhád",
        "coord": {
            "lon": 18.530239,
            "lat": 46.29921
        }
    },
    {
        "id": 3054811,
        "name": "Bőny",
        "coord": {
            "lon": 17.86977,
            "lat": 47.650002
        }
    },
    {
        "id": 3054823,
        "name": "Bolyok",
        "coord": {
            "lon": 19.83333,
            "lat": 47.966671
        }
    },
    {
        "id": 3054828,
        "name": "Bóly",
        "coord": {
            "lon": 18.51833,
            "lat": 45.96722
        }
    },
    {
        "id": 3054839,
        "name": "Bolgártelep",
        "coord": {
            "lon": 19.200001,
            "lat": 47.616669
        }
    },
    {
        "id": 3054850,
        "name": "Boldog",
        "coord": {
            "lon": 19.688391,
            "lat": 47.602859
        }
    },
    {
        "id": 3054853,
        "name": "Bölcske",
        "coord": {
            "lon": 18.96736,
            "lat": 46.74102
        }
    },
    {
        "id": 3054863,
        "name": "Bokod",
        "coord": {
            "lon": 18.247431,
            "lat": 47.491859
        }
    },
    {
        "id": 3054874,
        "name": "Böhönye",
        "coord": {
            "lon": 17.38036,
            "lat": 46.413448
        }
    },
    {
        "id": 3054878,
        "name": "Bogyiszló",
        "coord": {
            "lon": 18.82962,
            "lat": 46.386379
        }
    },
    {
        "id": 3054940,
        "name": "Bodakút",
        "coord": {
            "lon": 19.133329,
            "lat": 47.033329
        }
    },
    {
        "id": 3054945,
        "name": "Bodajk",
        "coord": {
            "lon": 18.23312,
            "lat": 47.323521
        }
    },
    {
        "id": 3055030,
        "name": "Bicske",
        "coord": {
            "lon": 18.644039,
            "lat": 47.484192
        }
    },
    {
        "id": 3055038,
        "name": "Biatorbágy",
        "coord": {
            "lon": 18.81892,
            "lat": 47.4706
        }
    },
    {
        "id": 3055069,
        "name": "Berzence",
        "coord": {
            "lon": 17.1481,
            "lat": 46.20908
        }
    },
    {
        "id": 3055091,
        "name": "Berhida",
        "coord": {
            "lon": 18.12948,
            "lat": 47.111309
        }
    },
    {
        "id": 3055108,
        "name": "Beremend",
        "coord": {
            "lon": 18.432631,
            "lat": 45.79108
        }
    },
    {
        "id": 3055130,
        "name": "Bercel",
        "coord": {
            "lon": 19.40715,
            "lat": 47.870571
        }
    },
    {
        "id": 3055151,
        "name": "Belső Új Sor",
        "coord": {
            "lon": 19.549999,
            "lat": 46.216671
        }
    },
    {
        "id": 3055196,
        "name": "Beled",
        "coord": {
            "lon": 17.092939,
            "lat": 47.465939
        }
    },
    {
        "id": 3055255,
        "name": "Becsehely",
        "coord": {
            "lon": 16.7771,
            "lat": 46.447552
        }
    },
    {
        "id": 3055281,
        "name": "Bátya",
        "coord": {
            "lon": 18.954189,
            "lat": 46.487999
        }
    },
    {
        "id": 3055287,
        "name": "Bátonyterenye",
        "coord": {
            "lon": 19.840759,
            "lat": 47.96962
        }
    },
    {
        "id": 3055298,
        "name": "Bátaszék",
        "coord": {
            "lon": 18.72307,
            "lat": 46.193729
        }
    },
    {
        "id": 3055302,
        "name": "Báta",
        "coord": {
            "lon": 18.770269,
            "lat": 46.128639
        }
    },
    {
        "id": 3055368,
        "name": "Barcs",
        "coord": {
            "lon": 17.458611,
            "lat": 45.959999
        }
    },
    {
        "id": 3055399,
        "name": "Baranya county",
        "coord": {
            "lon": 18.25,
            "lat": 46.083328
        }
    },
    {
        "id": 3055410,
        "name": "Baracska",
        "coord": {
            "lon": 18.758539,
            "lat": 47.282249
        }
    },
    {
        "id": 3055414,
        "name": "Baracs",
        "coord": {
            "lon": 18.90658,
            "lat": 46.882019
        }
    },
    {
        "id": 3055497,
        "name": "Ballószög",
        "coord": {
            "lon": 19.570921,
            "lat": 46.86216
        }
    },
    {
        "id": 3055535,
        "name": "Balatonszárszó",
        "coord": {
            "lon": 17.824261,
            "lat": 46.82946
        }
    },
    {
        "id": 3055539,
        "name": "Balatonszabadi",
        "coord": {
            "lon": 18.137381,
            "lat": 46.893978
        }
    },
    {
        "id": 3055551,
        "name": "Balatonlelle",
        "coord": {
            "lon": 17.694981,
            "lat": 46.78318
        }
    },
    {
        "id": 3055556,
        "name": "Balatonkenese",
        "coord": {
            "lon": 18.10671,
            "lat": 47.040192
        }
    },
    {
        "id": 3055563,
        "name": "Balatonfűzfő",
        "coord": {
            "lon": 18.049999,
            "lat": 47.066669
        }
    },
    {
        "id": 3055566,
        "name": "Balatonfüred",
        "coord": {
            "lon": 17.87187,
            "lat": 46.96188
        }
    },
    {
        "id": 3055568,
        "name": "Balatonföldvár",
        "coord": {
            "lon": 17.881229,
            "lat": 46.852551
        }
    },
    {
        "id": 3055575,
        "name": "Balatonfenyves",
        "coord": {
            "lon": 17.492229,
            "lat": 46.71542
        }
    },
    {
        "id": 3055584,
        "name": "Balatonboglár",
        "coord": {
            "lon": 17.64414,
            "lat": 46.775249
        }
    },
    {
        "id": 3055586,
        "name": "Balatonberény",
        "coord": {
            "lon": 17.320129,
            "lat": 46.70702
        }
    },
    {
        "id": 3055590,
        "name": "Balatonalmádi",
        "coord": {
            "lon": 18.02076,
            "lat": 47.035259
        }
    },
    {
        "id": 3055601,
        "name": "Balassagyarmat",
        "coord": {
            "lon": 19.296141,
            "lat": 48.07296
        }
    },
    {
        "id": 3055603,
        "name": "Baláczaidűlő",
        "coord": {
            "lon": 17.433331,
            "lat": 47.099998
        }
    },
    {
        "id": 3055623,
        "name": "Bakonyszentlászló",
        "coord": {
            "lon": 17.803209,
            "lat": 47.389011
        }
    },
    {
        "id": 3055644,
        "name": "Bakonycsernye",
        "coord": {
            "lon": 18.07509,
            "lat": 47.323952
        }
    },
    {
        "id": 3055674,
        "name": "Bajna",
        "coord": {
            "lon": 18.5972,
            "lat": 47.653999
        }
    },
    {
        "id": 3055685,
        "name": "Baja",
        "coord": {
            "lon": 18.95639,
            "lat": 46.174961
        }
    },
    {
        "id": 3055686,
        "name": "Baj",
        "coord": {
            "lon": 18.3641,
            "lat": 47.645931
        }
    },
    {
        "id": 3055721,
        "name": "Bag",
        "coord": {
            "lon": 19.48333,
            "lat": 47.633331
        }
    },
    {
        "id": 3055730,
        "name": "Badacsonytomaj",
        "coord": {
            "lon": 17.513849,
            "lat": 46.80711
        }
    },
    {
        "id": 3055744,
        "name": "Bács-Kiskun county",
        "coord": {
            "lon": 19.41667,
            "lat": 46.5
        }
    },
    {
        "id": 3055753,
        "name": "Bácsborsód",
        "coord": {
            "lon": 19.15806,
            "lat": 46.098701
        }
    },
    {
        "id": 3055757,
        "name": "Bácsbokod",
        "coord": {
            "lon": 19.15621,
            "lat": 46.125
        }
    },
    {
        "id": 3055760,
        "name": "Bácsalmás",
        "coord": {
            "lon": 19.3326,
            "lat": 46.12648
        }
    },
    {
        "id": 3055772,
        "name": "Bábolna",
        "coord": {
            "lon": 17.97798,
            "lat": 47.643608
        }
    },
    {
        "id": 3055776,
        "name": "Babócsa",
        "coord": {
            "lon": 17.343321,
            "lat": 46.04155
        }
    },
    {
        "id": 3055827,
        "name": "Ásványráró",
        "coord": {
            "lon": 17.494181,
            "lat": 47.827332
        }
    },
    {
        "id": 3055835,
        "name": "Ásotthalom",
        "coord": {
            "lon": 19.78334,
            "lat": 46.19875
        }
    },
    {
        "id": 3055878,
        "name": "Aranyhegy",
        "coord": {
            "lon": 19.23333,
            "lat": 47.299999
        }
    },
    {
        "id": 3055895,
        "name": "Apostagi Tanyák",
        "coord": {
            "lon": 18.950001,
            "lat": 46.883331
        }
    },
    {
        "id": 3055896,
        "name": "Apostag",
        "coord": {
            "lon": 18.962099,
            "lat": 46.88208
        }
    },
    {
        "id": 3055901,
        "name": "Apc",
        "coord": {
            "lon": 19.69429,
            "lat": 47.794189
        }
    },
    {
        "id": 3055969,
        "name": "Angyalisziget",
        "coord": {
            "lon": 18.950001,
            "lat": 47.166672
        }
    },
    {
        "id": 3056108,
        "name": "Alsónémedi",
        "coord": {
            "lon": 19.15843,
            "lat": 47.315239
        }
    },
    {
        "id": 3056159,
        "name": "Alsókörtvélyes",
        "coord": {
            "lon": 18.633329,
            "lat": 46.883331
        }
    },
    {
        "id": 3056211,
        "name": "Alsógalla",
        "coord": {
            "lon": 18.41646,
            "lat": 47.572449
        }
    },
    {
        "id": 3056235,
        "name": "Alsódolina",
        "coord": {
            "lon": 18.73333,
            "lat": 46.183331
        }
    },
    {
        "id": 3056290,
        "name": "Almásfüzitő",
        "coord": {
            "lon": 18.26153,
            "lat": 47.72752
        }
    },
    {
        "id": 3056326,
        "name": "Albertirsa",
        "coord": {
            "lon": 19.616859,
            "lat": 47.243149
        }
    },
    {
        "id": 3056331,
        "name": "Alap",
        "coord": {
            "lon": 18.689381,
            "lat": 46.799141
        }
    },
    {
        "id": 3056347,
        "name": "Akasztó",
        "coord": {
            "lon": 19.204229,
            "lat": 46.691669
        }
    },
    {
        "id": 3056355,
        "name": "Ajkai Tanyák",
        "coord": {
            "lon": 17.566669,
            "lat": 47.099998
        }
    },
    {
        "id": 3056357,
        "name": "Ajka",
        "coord": {
            "lon": 17.55892,
            "lat": 47.101959
        }
    },
    {
        "id": 3056361,
        "name": "Agyagbánya",
        "coord": {
            "lon": 19.450001,
            "lat": 47.349998
        }
    },
    {
        "id": 3056380,
        "name": "Ágfalva",
        "coord": {
            "lon": 16.516581,
            "lat": 47.689911
        }
    },
    {
        "id": 3056391,
        "name": "Ágasegyháza",
        "coord": {
            "lon": 19.45208,
            "lat": 46.84026
        }
    },
    {
        "id": 3056414,
        "name": "Adony",
        "coord": {
            "lon": 18.864929,
            "lat": 47.1194
        }
    },
    {
        "id": 3056418,
        "name": "Ádánd",
        "coord": {
            "lon": 18.164419,
            "lat": 46.85931
        }
    },
    {
        "id": 3056424,
        "name": "Adács",
        "coord": {
            "lon": 19.976959,
            "lat": 47.692101
        }
    },
    {
        "id": 3056436,
        "name": "Acsa",
        "coord": {
            "lon": 19.387951,
            "lat": 47.79425
        }
    },
    {
        "id": 3056437,
        "name": "Ács",
        "coord": {
            "lon": 18.011311,
            "lat": 47.712311
        }
    },
    {
        "id": 3056445,
        "name": "Abda",
        "coord": {
            "lon": 17.54488,
            "lat": 47.694641
        }
    },
    {
        "id": 3056450,
        "name": "Aba",
        "coord": {
            "lon": 18.521721,
            "lat": 47.029072
        }
    }
];

export default cities;