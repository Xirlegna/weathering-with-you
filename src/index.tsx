import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import city from './store/reducers/city';
import citySearchPopup from './store/reducers/citySearchPopup';
import weather from './store/reducers/weather';
import App from './components/App';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    city,
    citySearchPopup,
    weather
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (<Provider store={store}><App /></Provider>);

ReactDOM.render(app, document.getElementById('root'));