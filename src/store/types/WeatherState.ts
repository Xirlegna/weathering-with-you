type WeatherState = {
    data: {
        main: string;
        description: string;
        temp: number;
        feelsLike: number;
        visibility: number;
        windSpeed: number;
    };
    loading: boolean;
    className: string;
}

export default WeatherState;