import * as actionTypes from '../actions/actionTypes';
import Weather from './Weather';

type WeatherAction = {
    type: typeof actionTypes.FETCH_WEATHER_START | typeof actionTypes.FETCH_WEATHER_SUCCESS;
    data: Weather
};

export default WeatherAction;