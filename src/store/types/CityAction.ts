import * as actionTypes from '../actions/actionTypes';

type CityAction = {
    type: typeof actionTypes.SET_CITY;
    city: string
};

export default CityAction;