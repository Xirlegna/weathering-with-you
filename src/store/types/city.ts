type State = {
    id: number;
    name: string;
    coord: {
        lon: number;
        lat: number;
    };
};

type Action = {
    type: string;
};

const initialState: State = {
    id: 3054638,
    name: "Budapest",
    coord: {
        lon: 19.08333,
        lat: 47.5
    }
}

const reducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        default: return state;
    }
};

export default reducer;