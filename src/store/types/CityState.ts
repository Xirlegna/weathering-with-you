type CityState = {
    id: number;
    name: string;
    coord: {
        lon: number;
        lat: number;
    };
};

export default CityState;