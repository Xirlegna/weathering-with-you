type CitySearchPopupState = {
    display: boolean;
}

export default CitySearchPopupState;