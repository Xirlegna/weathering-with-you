type Weather = {
    weather: {
        main: string;
        description: string;
    }[];
    main: {
        temp: number;
        feels_like: number;
    };
    visibility: number;
    wind: {
        speed: number;
    };
};

export default Weather;