import * as actionTypes from '../actions/actionTypes';
import Action from '../types/CityAction';
import State from '../types/CityState';
import cities from '../../shared/cities';

const initialState: State = {
    id: 3054638,
    name: "Budapest",
    coord: {
        lon: 19.08333,
        lat: 47.5
    }
}

const setCity = (state: State, action: Action): State => {
    const city = cities.filter((city) => city.name.toLowerCase() === action.city.toLowerCase());

    if (city.length > 0) {
        return { ...city[0] };
    }

    return state;
};

const reducer = (state: State = initialState, action: Action) => {
    switch (action.type) {
        case actionTypes.SET_CITY: return setCity(state, action);
        default: return state;
    }
};

export default reducer;