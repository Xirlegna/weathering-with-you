import * as actionTypes from '../actions/actionTypes';

type State = {
    display: boolean;
}

type Action = {
    type: typeof actionTypes.CITY_SEARCH_SHOW | typeof actionTypes.CITY_SEARCH_HIDE;
}

const initialState: State = {
    display: false
};

const citySearchShow = (state: State): State => {
    return {
        ...state,
        display: true
    };
};

const citySearchHide = (state: State): State => {
    return {
        ...state,
        display: false
    };
};

const reducer = (state: State = initialState, action: Action): State => {
    switch (action.type) {
        case actionTypes.CITY_SEARCH_SHOW: return citySearchShow(state);
        case actionTypes.CITY_SEARCH_HIDE: return citySearchHide(state);
        default: return state;
    }
};

export default reducer;