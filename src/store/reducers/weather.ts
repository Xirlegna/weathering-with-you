import * as actionTypes from '../actions/actionTypes';
import Action from '../types/WeatherAction';
import State from '../types/WeatherState';

const initialState: State = {
    data: {
        main: '',
        description: '',
        temp: NaN,
        feelsLike: NaN,
        visibility: NaN,
        windSpeed: NaN
    },
    loading: false,
    className: 'container--clear'
};

const fetchWeatherStart = (state: State): State => {
    return {
        ...state,
        loading: true
    };
};

const fetchWeatherSuccess = (state: State, action: Action): State => {
    const fog: string[] = ['Mist', 'Smoke', 'Haze', 'Dust', 'Sand', 'Dust', 'Ash'];
    let className: string = action.data.weather[0].main;
    if (fog.includes(className)) {
        className = 'Fog';
    }

    return {
        ...state,
        loading: false,
        className: `container--${className.toLowerCase()}`,
        data: {
            main: action.data.weather[0].main,
            description: action.data.weather[0].description,
            temp: action.data.main.temp,
            feelsLike: action.data.main.feels_like,
            visibility: action.data.visibility,
            windSpeed: action.data.wind.speed
        }
    };
};

const reducer = (state: State = initialState, action: Action): State => {
    switch (action.type) {
        case actionTypes.FETCH_WEATHER_START: return fetchWeatherStart(state);
        case actionTypes.FETCH_WEATHER_SUCCESS: return fetchWeatherSuccess(state, action);
        default: return state;
    }
};

export default reducer;
