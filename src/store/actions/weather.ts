import * as actionTypes from './actionTypes';
import Weather from '../types/Weather';
import axios from '../../axios-weather';

export const fetchWeatherStart = (): { type: string } => {
    return {
        type: actionTypes.FETCH_WEATHER_START
    };
};

export const fetchWeatherSuccess = (weather: Weather): { type: string, data: Weather } => {
    return {
        type: actionTypes.FETCH_WEATHER_SUCCESS,
        data: weather
    };
};

export const fetchWeather = (id: number) => {
    return (dispatch: any) => {
        dispatch(fetchWeatherStart());
        axios.get(`weather?id=${id}&units=metric&lang=hu&appid=8a0300d4e3aabc65ca87f4089ab0d6fb`)
            .then(response => {
                dispatch(fetchWeatherSuccess(response.data));
            })
            .catch(error => {
                console.log(error);
            });
    };
};