export const CITY_SEARCH_SHOW: string = 'CITY_SEARCH_SHOW';
export const CITY_SEARCH_HIDE: string = 'CITY_SEARCH_HIDE';

export const FETCH_WEATHER_START: string = 'FETCH_WEATHER_START';
export const FETCH_WEATHER_SUCCESS: string = 'FETCH_WEATHER_SUCCESS';

export const SET_CITY: string = 'SET_CITY';