import * as actionTypes from './actionTypes';

export const setCity = (city: string): { type: string, city: string } => {
    return {
        type: actionTypes.SET_CITY,
        city
    };
}