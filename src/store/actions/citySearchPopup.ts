import * as actionTypes from './actionTypes';

export const citySearchShow = (): { type: string } => {
    return {
        type: actionTypes.CITY_SEARCH_SHOW,
    };
}

export const citySearchHide = (): { type: string } => {
    return {
        type: actionTypes.CITY_SEARCH_HIDE,
    };
}